import 'dart:io';

final lib = Directory('lib');
void main() {
  final items = lib.listSync(recursive: true);
  for (var event in items) {
    if (event is File) {
      if (event.path.endsWith('.dart')) {
        String content = event.readAsStringSync();
        final r = RegExp(r'context.l10n.(.+),');
        for (var m in r.allMatches(content)) {
          String key = m.group(1)!;
          key = 'k' + key.substring(0, 1).toUpperCase() + key.substring(1);
          content = content.replaceAll(m.group(0)!, 'context.l10n.$key,');
        }

        event.writeAsString(content, flush: true);
      }
    }
  }
}
