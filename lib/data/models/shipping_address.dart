class ShippingAddress {
  int id;
  String address;
  String province;
  String city;
  String district;
  String subDistrict;
  String zipCode;
  double latitude;
  double longitude;
  bool isDefault;
  int areasId;

  ShippingAddress(
      {required this.id,
        required this.address,
        required this.province,
        required this.city,
        required this.district,
        required this.subDistrict,
        required this.zipCode,
        required this.latitude,
        required this.longitude,
        required this.isDefault,
        required this.areasId});

  factory ShippingAddress.fromJson(Map<String, dynamic> json) =>
      ShippingAddress(
        id: json['id'],
        address: json['address'],
        province: json['province'],
        city: json['city'],
        district: json['district'],
        subDistrict: json['subdistrict'],
        zipCode: json['zipcode'],
        latitude: json['latitude'],
        longitude: json['longitude'],
        isDefault: json['default'],
        areasId: json['areaId'],
      );

  Map<String, dynamic> toJson() => {
    'id': id,
    'address': address,
    'province': province,
    'city': city,
    'district': district,
    'subdistrict': subDistrict,
    'zipcode': zipCode,
    'latitude': latitude,
    'longitude': longitude,
    'default': isDefault,
    'areaid': areasId,
  };
}


//
