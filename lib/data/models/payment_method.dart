import 'package:selon_market/data/models/list_hint_payment.dart';

class PaymentMethod {
  final String bank;
  final String description;
  final String channelId;
  final String logo;
  final int admin;
  final List<HintPaymentData> listHintPayment;

  const PaymentMethod({
    required this.logo,
    required this.bank,
    required this.description,
    required this.channelId,
    required this.admin,
    required this.listHintPayment,
  });
}
