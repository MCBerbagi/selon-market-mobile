class ProductOwner {
  int? id;
  String? name;
  String? profession;
  String? category;
  int? status;

  ProductOwner(
      {this.id, this.name, this.profession, this.category, this.status});

  ProductOwner.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    profession = json['profession'];
    category = json['category'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['profession'] = this.profession;
    data['category'] = this.category;
    data['status'] = this.status;
    return data;
  }
}
