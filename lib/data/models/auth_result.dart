import 'package:selon_market/selon_market.dart';

class AuthResult {
  String userId;
  String authorization;
  User user;

  AuthResult({
    required this.userId,
    required this.authorization,
    required this.user,
  });

  factory AuthResult.fromJson(Map<String, dynamic> json) => AuthResult(
        authorization: json['authorization'],
        userId: json['userid'],
        user: User.fromJson(json['user']),
      );

  Map<String, dynamic> toJson() => {
        'userid': userId,
        'authorization': authorization,
        'user': user.toJson(),
      };
}
