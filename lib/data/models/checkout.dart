import 'package:selon_market/selon_market.dart';

class Checkout {
  Outlet outlet;
  List<CheckoutItem> items;
  Checkout({
    required this.outlet,
    required this.items,
  });
}

class CheckoutItem {
  Cart cart;
  int qty;

  CheckoutItem({
    required this.qty,
    required this.cart,
  });
}

class CheckoutResult {
  final String virtualAccount;
  final String channelId;
  final double transactionAmount;
  final DateTime expiredDate;

  CheckoutResult({
    required this.virtualAccount,
    required this.channelId,
    required this.transactionAmount,
    required this.expiredDate,
  });

  factory CheckoutResult.fromJson(Map<String, dynamic> json) => CheckoutResult(
        virtualAccount: json['virtual_account'],
        channelId: json['channel_id'],
        transactionAmount: double.parse(json['transaction_amount']),
        expiredDate: DateTime.parse(json['expired_date']),
      );
}
