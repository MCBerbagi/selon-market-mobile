import 'package:selon_market/selon_market.dart';

class Productfecthall {
  List<ProductListAll>? productList;
  String? message;
  String? timestamp;

  Productfecthall({this.productList, this.message, this.timestamp});

  Productfecthall.fromJson(Map<String, dynamic> json) {
    if (json['product_list'] != null) {
      productList = <ProductListAll>[];
      json['product_list'].forEach((v) {
        print("========sue=======");
        print(v);
        productList!.add(new ProductListAll.fromJson(v));
      });
    }
    message = json['message'];
    timestamp = json['timestamp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.productList != null) {
      data['product_list'] = this.productList!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['timestamp'] = this.timestamp;
    return data;
  }
}

class ProductListAll {
  String? id;
  int? productId;
  String? name;
  String? category;
  List<ProductOwner>? productOwner;
  List<Outlet>? outlet;
  List<ProductMedia>? productMedia;
  String? description;
  double? price;
  String? stockType;
  int? stockQty;
  bool? promo;
  double? discount;
  double? rating;
  String? createDate;
  String? lstUpdate;

  ProductListAll(
      {this.id,
      this.productId,
      this.name,
      this.category,
      this.productOwner,
      this.outlet,
      this.productMedia,
      this.description,
      this.price,
      this.stockType,
      this.stockQty,
      this.promo,
      this.discount,
      this.rating,
      this.createDate,
      this.lstUpdate});

  ProductListAll.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    productId = json['product_id'];
    name = json['name'];
    category = json['category'];
    if (json['product_owner'] != null) {
      productOwner = <ProductOwner>[];
      json['product_owner'].forEach((v) {
        productOwner!.add(new ProductOwner.fromJson(v));
      });
    }
    if (json['outlet'] != null) {
      outlet = <Outlet>[];
      json['outlet'].forEach((v) {
        outlet!.add(new Outlet.fromJson(v));
      });
    }
    if (json['product_media'] != null) {
      productMedia = <ProductMedia>[];
      json['product_media'].forEach((v) {
        productMedia!.add(new ProductMedia.fromJson(v));
      });
    }
    description = json['description'];
    price = json['price'];
    stockType = json['stock_type'];
    stockQty = json['stock_qty'];
    promo = json['promo'];
    discount = json['discount'];
    rating = json['rating'];
    createDate = json['create_date'];
    lstUpdate = json['lst_update'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['product_id'] = this.productId;
    data['name'] = this.name;
    data['category'] = this.category;
    if (this.productOwner != null) {
      data['product_owner'] = this.productOwner!.map((v) => v.toJson()).toList();
    }
    if (this.outlet != null) {
      data['outlet'] = this.outlet!.map((v) => v.toJson()).toList();
    }
    if (this.productMedia != null) {
      data['product_media'] = this.productMedia!.map((v) => v.toJson()).toList();
    }
    data['description'] = this.description;
    data['price'] = this.price;
    data['stock_type'] = this.stockType;
    data['stock_qty'] = this.stockQty;
    data['promo'] = this.promo;
    data['discount'] = this.discount;
    data['rating'] = this.rating;
    data['create_date'] = this.createDate;
    data['lst_update'] = this.lstUpdate;
    return data;
  }
}
