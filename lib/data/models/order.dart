class Order {
  Order({
    required this.id,
    required this.invoice,
    required this.customerName,
    required this.customerPhone,
    required this.customerEmail,
    required this.transactionAmount,
    required this.paymentChannel,
    required this.adminFee,
    required this.paymentDate,
    required this.providerName,
    required this.providerService,
    required this.totalWeight,
    required this.deliveryPrice,
    required this.trackingId,
    required this.customerAddress,
    required this.productOrdered,
    required this.outletName,
    required this.orderDate,
    required this.lastUpdate,
    required this.orderStatus,
    required this.paymentStatus,
    required this.deliveryStatus,
  });

  final int id;
  final String invoice;
  final String customerName;
  final String customerPhone;
  final String customerEmail;
  final int transactionAmount;
  final String paymentChannel;
  final int adminFee;
  final DateTime paymentDate;
  final String providerName;
  final String providerService;
  final int totalWeight;
  final int deliveryPrice;
  final String trackingId;
  final CustomerAddress? customerAddress;
  final List<ProductOrdered> productOrdered;
  final String outletName;
  final DateTime orderDate;
  final DateTime lastUpdate;
  final String orderStatus;
  final String paymentStatus;
  final String deliveryStatus;

  factory Order.fromJson(Map<String, dynamic> json) => Order(
        id: json["id"],
        invoice: json["invoice"],
        customerName: json["customer_name"],
        customerPhone: json["customer_phone"],
        customerEmail: json["customer_email"],
        transactionAmount: json["transaction_amount"],
        paymentChannel: json["payment_channel"],
        adminFee: json["admin_fee"],
        paymentDate: DateTime.parse(json["payment_date"]),
        providerName: json["provider_name"],
        providerService: json["provider_service"],
        totalWeight: json["total_weight"],
        deliveryPrice: json["delivery_price"],
        trackingId: json["tracking_id"],
        customerAddress: json["customer_address"] == null
            ? null
            : CustomerAddress.fromJson(json["customer_address"]),
        productOrdered: List<ProductOrdered>.from(
            json["product_ordered"].map((x) => ProductOrdered.fromJson(x))),
        outletName: json["outlet_name"],
        orderDate: DateTime.parse(json["order_date"]),
        lastUpdate: DateTime.parse(json["last_update"]),
        orderStatus: json["order_status"],
        paymentStatus: json["payment_status"],
        deliveryStatus: json["delivery_status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "invoice": invoice,
        "customer_name": customerName,
        "customer_phone": customerPhone,
        "customer_email": customerEmail,
        "transaction_amount": transactionAmount,
        "payment_channel": paymentChannel,
        "admin_fee": adminFee,
        "payment_date": paymentDate.toIso8601String(),
        "provider_name": providerName,
        "provider_service": providerService,
        "total_weight": totalWeight,
        "delivery_price": deliveryPrice,
        "tracking_id": trackingId,
        "customer_address": customerAddress?.toJson(),
        "product_ordered":
            List<dynamic>.from(productOrdered.map((x) => x.toJson())),
        "outlet_name": outletName,
        "order_date": orderDate.toIso8601String(),
        "last_update": lastUpdate.toIso8601String(),
        "order_status": orderStatus,
        "payment_status": paymentStatus,
        "delivery_status": deliveryStatus,
      };
}

class CustomerAddress {
  CustomerAddress({
    required this.id,
    required this.address,
    required this.province,
    required this.city,
    required this.district,
    required this.subdistrict,
    required this.longitude,
    required this.latitude,
    required this.zipcode,
    required this.customerAddressDefault,
  });

  final int id;
  final String address;
  final String province;
  final String city;
  final String district;
  final String subdistrict;
  final double longitude;
  final double latitude;
  final String zipcode;
  final bool customerAddressDefault;

  factory CustomerAddress.fromJson(Map<String, dynamic> json) =>
      CustomerAddress(
        id: json["id"],
        address: json["address"],
        province: json["province"],
        city: json["city"],
        district: json["district"],
        subdistrict: json["subdistrict"],
        longitude: json["longitude"].toDouble(),
        latitude: json["latitude"].toDouble(),
        zipcode: json["zipcode"],
        customerAddressDefault: json["default"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "address": address,
        "province": province,
        "city": city,
        "district": district,
        "subdistrict": subdistrict,
        "longitude": longitude,
        "latitude": latitude,
        "zipcode": zipcode,
        "default": customerAddressDefault,
      };
}

class ProductOrdered {
  ProductOrdered({
    required this.productId,
    required this.qty,
    required this.productPrice,
    required this.totalPrice,
    required this.productName,
    required this.productDescription,
    required this.owner,
    required this.originalUrl,
    required this.thumbnailUrl,
    required this.note,
  });

  final int productId;
  final int qty;
  final int productPrice;
  final int totalPrice;
  final String productName;
  final String productDescription;
  final String owner;
  final String originalUrl;
  final String thumbnailUrl;
  final String note;

  factory ProductOrdered.fromJson(Map<String, dynamic> json) => ProductOrdered(
        productId: json["product_id"],
        qty: json["qty"],
        productPrice: json["product_price"],
        totalPrice: json["total_price"],
        productName: json["product_name"],
        productDescription: json["product_description"],
        owner: json["owner"],
        originalUrl: json["original_url"] ?? '',
        thumbnailUrl: json["thumbnail_url"] ?? '',
        note: json["note"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "product_id": productId,
        "qty": qty,
        "product_price": productPrice,
        "total_price": totalPrice,
        "product_name": productName,
        "product_description": productDescription,
        "owner": owner,
        "original_url": originalUrl,
        "thumbnail_url": thumbnailUrl,
        "note": note,
      };
}
