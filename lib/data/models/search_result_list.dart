class SearchResultList {
  String? status, code, message;
  List<DataSearch>? data;

  SearchResultList({this.status, this.code, this.data, this.message});

  SearchResultList.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    code = json['code'];
    if (json['data'] != null) {
      data = <DataSearch>[];
      json['data'].forEach((v) {
        data!.add(new DataSearch.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class DataSearch {
  String? label, value, cityName, suburbName, areaName;
  int? orderList;

  DataSearch(
      {this.label,
        this.value,
        this.cityName,
        this.suburbName,
        this.areaName,
        this.orderList});

  DataSearch.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    value = json['value'];
    cityName = json['city_name'];
    suburbName = json['suburb_name'];
    areaName = json['area_name'];
    orderList = json['order_list'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['label'] = this.label;
    data['value'] = this.value;
    data['city_name'] = this.cityName;
    data['suburb_name'] = this.suburbName;
    data['area_name'] = this.areaName;
    data['order_list'] = this.orderList;
    return data;
  }
}
