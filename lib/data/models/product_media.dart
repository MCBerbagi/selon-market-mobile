class ProductMedia {
  int? mediaId;
  String? mediaType;
  String? mediaUrl;
  String? thumbnailUrl;
  String? ceateDate;
  String? lastUpdate;

  ProductMedia(
      {this.mediaId,
      this.mediaType,
      this.mediaUrl,
      this.thumbnailUrl,
      this.ceateDate,
      this.lastUpdate});

  ProductMedia.fromJson(Map<String, dynamic> json) {
    mediaId = json['media_id'];
    mediaType = json['media_type'];
    mediaUrl = json['media_url'];
    thumbnailUrl = json['thumbnail_url'];
    ceateDate = json['ceate_date'];
    lastUpdate = json['last_update'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['media_id'] = this.mediaId;
    data['media_type'] = this.mediaType;
    data['media_url'] = this.mediaUrl;
    data['thumbnail_url'] = this.thumbnailUrl;
    data['ceate_date'] = this.ceateDate;
    data['last_update'] = this.lastUpdate;
    return data;
  }
}
