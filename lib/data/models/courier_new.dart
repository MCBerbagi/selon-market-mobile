class CourierNew {
  String? status;
  String? code;
  String? rule;
  String? originArea;
  Data? data;
  String? message;
  String? destintationArea;

  CourierNew(
      {this.status,
        this.code,
        this.rule,
        this.originArea,
        this.data,
        this.message,
        this.destintationArea});

  CourierNew.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    code = json['code'];
    rule = json['rule'];
    originArea = json['originArea'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    destintationArea = json['destintationArea'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['code'] = this.code;
    data['rule'] = this.rule;
    data['originArea'] = this.originArea;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['message'] = this.message;
    data['destintationArea'] = this.destintationArea;
    return data;
  }
}

class Data {
  Logistic? logistic;

  Data({this.logistic});

  Data.fromJson(Map<String, dynamic> json) {
    logistic = json['logistic'] != null
        ? new Logistic.fromJson(json['logistic'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.logistic != null) {
      data['logistic'] = this.logistic!.toJson();
    }
    return data;
  }
}

class Logistic {
  List<LogisticService>? express;
  List<LogisticService>? regular;
  List<LogisticService>? trucking;
  List<LogisticService>? instant;
  List<LogisticService>? sameDay;

  Logistic(
      {this.express, this.regular, this.trucking, this.instant, this.sameDay});

  Logistic.fromJson(Map<String, dynamic> json) {
    if (json['express'] != null) {
      express = <LogisticService>[];
      json['express'].forEach((v) {
        express!.add(new LogisticService.fromJson(v));
      });
    }
    if (json['regular'] != null) {
      regular = <LogisticService>[];
      json['regular'].forEach((v) {
        regular!.add(new LogisticService.fromJson(v));
      });
    }
    if (json['trucking'] != null) {
      trucking = <LogisticService>[];
      json['trucking'].forEach((v) {
        trucking!.add(new LogisticService.fromJson(v));
      });
    }
    if (json['instant'] != null) {
      instant = <LogisticService>[];
      json['instant'].forEach((v) {
        instant!.add(new LogisticService.fromJson(v));
      });
    }
    if (json['same day'] != null) {
      sameDay =  <LogisticService>[];
      json['same day'].forEach((v) {
        sameDay!.add(new LogisticService.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.express != null) {
      data['express'] = this.express!.map((v) => v.toJson()).toList();
    }
    if (this.regular != null) {
      data['regular'] = this.regular!.map((v) => v.toJson()).toList();
    }
    if (this.trucking != null) {
      data['trucking'] = this.trucking!.map((v) => v.toJson()).toList();
    }
    if (this.instant != null) {
      data['instant'] = this.instant!.map((v) => v.toJson()).toList();
    }
    if (this.sameDay != null) {
      data['same day'] = this.sameDay!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LogisticService {
  int? rateId;
  int? showId;
  String? name;
  String? rateName;
  int? stopOrigin;
  int? stopDestination;
  String? logoUrl;
  int? weight;
  double? volumeWeight;
  int? logisticId;
  int? finalWeight;
  int? itemPrice;
  int? itemPrice2;
  int? finalRate;
  int? insuranceRate;
  int? compulsoryInsurance;
  int? liability;
  int? discount;
  int? minDay;
  int? maxDay;
  int? pickupAgent;
  bool? isHubless;

  LogisticService(
      {this.rateId,
        this.showId,
        this.name,
        this.rateName,
        this.stopOrigin,
        this.stopDestination,
        this.logoUrl,
        this.weight,
        this.volumeWeight,
        this.logisticId,
        this.finalWeight,
        this.itemPrice,
        this.itemPrice2,
        this.finalRate,
        this.insuranceRate,
        this.compulsoryInsurance,
        this.liability,
        this.discount,
        this.minDay,
        this.maxDay,
        this.pickupAgent,
        this.isHubless});

  LogisticService.fromJson(Map<String, dynamic> json) {
    rateId = json['rate_id'];
    showId = json['show_id'];
    name = json['name'];
    rateName = json['rate_name'];
    stopOrigin = json['stop_origin'];
    stopDestination = json['stop_destination'];
    logoUrl = json['logo_url'];
    weight = json['weight'];
    volumeWeight = json['volumeWeight'];
    logisticId = json['logistic_id'];
    finalWeight = json['finalWeight'];
    itemPrice = json['itemPrice'];
    itemPrice = json['item_price'];
    finalRate = json['finalRate'];
    insuranceRate = json['insuranceRate'];
    compulsoryInsurance = json['compulsory_insurance'];
    liability = json['liability'];
    discount = json['discount'];
    minDay = json['min_day'];
    maxDay = json['max_day'];
    pickupAgent = json['pickup_agent'];
    isHubless = json['is_hubless'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rate_id'] = this.rateId;
    data['show_id'] = this.showId;
    data['name'] = this.name;
    data['rate_name'] = this.rateName;
    data['stop_origin'] = this.stopOrigin;
    data['stop_destination'] = this.stopDestination;
    data['logo_url'] = this.logoUrl;
    data['weight'] = this.weight;
    data['volumeWeight'] = this.volumeWeight;
    data['logistic_id'] = this.logisticId;
    data['finalWeight'] = this.finalWeight;
    data['itemPrice'] = this.itemPrice;
    data['item_price'] = this.itemPrice;
    data['finalRate'] = this.finalRate;
    data['insuranceRate'] = this.insuranceRate;
    data['compulsory_insurance'] = this.compulsoryInsurance;
    data['liability'] = this.liability;
    data['discount'] = this.discount;
    data['min_day'] = this.minDay;
    data['max_day'] = this.maxDay;
    data['pickup_agent'] = this.pickupAgent;
    data['is_hubless'] = this.isHubless;
    return data;
  }
}
