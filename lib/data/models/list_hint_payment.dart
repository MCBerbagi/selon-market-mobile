class HintPaymentData {
  final String hintText;
  final List<dynamic> listHint;

  const HintPaymentData({required this.hintText, required this.listHint});
}
