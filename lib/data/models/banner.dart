class BannerResponse {
  String? status;
  int? code;
  List<BannerItem>? banners;
  String? message;
  String? timestamp;

  BannerResponse(
      {this.status, this.code, this.banners, this.message, this.timestamp});

  BannerResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    code = json['code'];
    if (json['banners'] != null) {
      banners = <BannerItem>[];
      json['banners'].forEach((v) {
        banners!.add(new BannerItem.fromJson(v));
      });
    }
    message = json['message'];
    timestamp = json['timestamp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['code'] = this.code;
    if (this.banners != null) {
      data['banners'] = this.banners!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['timestamp'] = this.timestamp;
    return data;
  }
}

class BannerItem {
  int? bannerId;
  String? title;
  int? promoType;
  String? promoUrl;
  String? imageUrl;

  BannerItem(
      {this.bannerId,
      this.title,
      this.promoType,
      this.promoUrl,
      this.imageUrl});

  BannerItem.fromJson(Map<String, dynamic> json) {
    bannerId = json['banner_id'];
    title = json['title'];
    promoType = json['promo_type'];
    promoUrl = json['promo_url'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['banner_id'] = this.bannerId;
    data['title'] = this.title;
    data['promo_type'] = this.promoType;
    data['promo_url'] = this.promoUrl;
    data['image_url'] = this.imageUrl;
    return data;
  }
}
