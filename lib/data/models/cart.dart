    import 'package:selon_market/selon_market.dart';

class CartFind {
  String? status;
  int? code;
  List<Cart>? cart;
  String? message;
  String? timestamp;

  CartFind({this.status, this.code, this.cart, this.message, this.timestamp});

  CartFind.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    code = json['code'];
    if (json['cart'] != null) {
      cart = <Cart>[];
      json['cart'].forEach((v) {
        cart!.add(new Cart.fromJson(v));
      });
    }
    message = json['message'];
    timestamp = json['timestamp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['code'] = this.code;
    if (this.cart != null) {
      data['cart'] = this.cart!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['timestamp'] = this.timestamp;
    return data;
  }
}

class Cart {
  int cartId;
  int productId;
  Outlet outlet;
  String name;
  String? category;
  ProductOwner productOwner;
  List<ProductMedia>? productMedia;
  String? description;
  double price;
  String? stockType;
  int qty;
  String? lastUpdate;

  Cart({
    required this.cartId,
    required this.productId,
    required this.outlet,
    required this.name,
    this.category,
    required this.productOwner,
    this.productMedia,
    this.description,
    this.price = 0,
    this.stockType,
    required this.qty,
    this.lastUpdate,
  });

  factory Cart.fromJson(Map<String, dynamic> json) => Cart(
        cartId: json['cart_id'],
        productId: json['product_id'],
        outlet: (json['outlet'] as List).map((e) => Outlet.fromJson(e)).first,
        name: json['name'],
        category: json['category'],
        productOwner: (json['product_owner'] as List)
            .map((e) => ProductOwner.fromJson(e))
            .first,
        productMedia: json['product_media'] == null
            ? []
            : (json['product_media'] as List)
                .map((e) => ProductMedia.fromJson(e))
                .toList(),
        description: json['description'],
        price: (json['price'] as num).toDouble(),
        stockType: json['stock_type'],
        qty: json['qty'] ?? 1,
        lastUpdate: json['last_update'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cart_id'] = this.cartId;
    data['product_id'] = this.productId;

    data['outlet'] = [this.outlet.toJson()];

    data['name'] = this.name;
    data['category'] = this.category;

    data['product_owner'] = [this.productOwner.toJson()];

    if (this.productMedia != null) {
      data['product_media'] =
          this.productMedia!.map((v) => v.toJson()).toList();
    }
    data['description'] = this.description;
    data['price'] = this.price;
    data['stock_type'] = this.stockType;
    data['qty'] = this.qty;
    data['last_update'] = this.lastUpdate;
    return data;
  }
}
