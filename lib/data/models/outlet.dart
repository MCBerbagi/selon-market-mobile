class Outlet {
  int? id;
  String? name;
  String? phone;
  String? email;
  String? address;
  String? province;
  String? city;
  String? district;
  String? subdistrict;
  String? zipcode;
  double? longitude;
  double? latitude;
  int? status;
  int? areaId;

  Outlet(
      {this.id,
        this.name,
        this.phone,
        this.email,
        this.address,
        this.province,
        this.city,
        this.district,
        this.subdistrict,
        this.zipcode,
        this.longitude,
        this.latitude,
        this.status,
        this.areaId});

  Outlet.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
    email = json['email'];
    address = json['address'];
    province = json['province'];
    city = json['city'];
    district = json['district'];
    subdistrict = json['subdistrict'];
    zipcode = json['zipcode'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['address'] = this.address;
    data['province'] = this.province;
    data['city'] = this.city;
    data['district'] = this.district;
    data['subdistrict'] = this.subdistrict;
    data['zipcode'] = this.zipcode;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['status'] = this.status;
    return data;
  }
}
