class Courier {
  final String name;
  final List<CourierService> services;

  Courier({
    required this.name,
    required this.services,
  });
}

class CourierService {
  final String name;
  final double price;
  final String description;

  CourierService({
    required this.name,
    required this.price,
    required this.description,
  });
}


