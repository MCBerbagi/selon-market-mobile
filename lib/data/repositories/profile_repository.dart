import 'package:flutter/foundation.dart';
import 'package:selon_market/selon_market.dart';

class ProfileRepository with ChangeNotifier {
  final authDataSource = AuthDataSource(preferences);

  User get currentUser => authDataSource.currentUser;
}
