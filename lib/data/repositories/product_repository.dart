import 'package:flutter/foundation.dart';
import 'package:selon_market/selon_market.dart';

class ProductRepository with ChangeNotifier {
  final ProductService service;
  ProductRepository({required this.service}) {
    refresh();
  }

  List<Product> _list = [];
  List<Product> get list => _list;
  Future<void> refresh() async {
    _list = await service.fetchAll(
      size: 10,
    );
    notifyListeners();
  }
}
