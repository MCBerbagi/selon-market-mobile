import 'package:flutter/foundation.dart';
import 'package:selon_market/data/models/courier_new.dart';
import 'package:selon_market/selon_market.dart';

class OrderRepository with ChangeNotifier {
  final OrderService service;
  final authDataSource = AuthDataSource(preferences);
  OrderRepository({required this.service}) {
    refresh();
  }

  Future<CheckoutResult?> checkout({
    required Checkout checkout,
    required ShippingAddress receiver,
    // required Courier courier,
    // required CourierService courierService,
    required LogisticService logisticService,
    required String channelId,
  }) async {
    final res = await service.checkout(
      userId: authDataSource.currentUserId,
      checkout: checkout,
      receiver: receiver,
      // courier: courier,
      // courierService: courierService,
      logisticService: logisticService,
      channelId: channelId,
    );
    if (res != null) {
      refresh();
    }

    return res;
  }

  List<Order> _orderList = [];
  List<Order> get orderList => _orderList;
  Future<void> refresh() async {
    _orderList = await service.orders(userId: authDataSource.currentUserId);
    notifyListeners();
  }
}
