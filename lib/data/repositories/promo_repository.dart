import 'package:flutter/foundation.dart';
import 'package:selon_market/selon_market.dart';

class PromoRepository with ChangeNotifier {
  final PromoService service;
  final authDataSource = AuthDataSource(preferences);

  PromoRepository({required this.service}) {
    refresh();
  }

  Future<void> refresh() async {
    _bannerList = await service.getBanners(
      userId: authDataSource.currentUserId,
    );

    notifyListeners();
  }

  List<BannerItem> _bannerList = [];
  List<BannerItem> get bannerList => _bannerList;
}
