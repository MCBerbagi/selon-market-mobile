import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:selon_market/selon_market.dart';

class ConfigRepository with ChangeNotifier {
  ConfigRepository() {
    Future.delayed(Duration(seconds: 3), () {
      _showSplashScreen = false;
      notifyListeners();
    });
  }

  Locale get locale {
    if (preferences.containsKey('locale')) {
      final sLocale = preferences.getString('locale')!;
      return Locale(sLocale);
    }

    return Locale('en');
  }

  Future<void> setLocale(String languageCode) async {
    await preferences.setString('locale', languageCode);
    notifyListeners();
  }

  bool get showOnBoarding {
    return preferences.getBool('show_onboarding') ?? true;
  }

  set showOnBoarding(bool value) {
    preferences.setBool('show_onboarding', value).then(
          (value) => notifyListeners(),
        );
  }

  bool _showSplashScreen = true;
  bool get showSplashScreen => _showSplashScreen;
}
