import 'package:flutter/foundation.dart';
import 'package:selon_market/selon_market.dart';

class AddressRepository with ChangeNotifier {
  final AddressService service;
  final authDataSource = AuthDataSource(preferences);

  AddressRepository({required this.service}) {
    refresh();
  }

  List<ShippingAddress> _addressList = [];
  List<ShippingAddress> get addressList => _addressList;
  Future<void> refresh() async {
    _addressList = await service.list(userId: authDataSource.currentUserId);
    notifyListeners();
  }

  Future<bool> add({required ShippingAddress address}) async {
    final res = await service.add(
      userId: authDataSource.currentUserId,
      address: address,
    );

    if (res) {
      refresh();
    }

    return res;
  }

  SearchResultList _searchResultList = SearchResultList();
  SearchResultList get searchResultList => _searchResultList;
  Future<void> searchByAddress({required ShippingAddress address}) async {
    _searchResultList = await service.search(district: address.district);
    DataSearch _dataSearch = _searchResultList.data!.firstWhere(
            (element) => element.label!.contains(address.city.split(" ")[1]));
    address.areasId = int.parse(_dataSearch.value!.split("|")[2]);

    add(address: address);
    notifyListeners();
  }

  Future<int> searchAreaId({required Outlet outlet}) async {
    AddressService service = AddressService(dio);

    _searchResultList = await service.search(district: outlet.subdistrict!);
    DataSearch _dataSearch = _searchResultList.data!.firstWhere(
            (element) => element.label!.contains(outlet.city!.split(" ")[1]));
    outlet.areaId = int.parse(_dataSearch.value!.split("|")[2]);
    return outlet.areaId!;
  }
}
