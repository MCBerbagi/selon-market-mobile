import 'package:flutter/foundation.dart';
import 'package:selon_market/data/datasource/auth_datasource.dart';
import 'package:selon_market/selon_market.dart';

enum AuthState {
  authenticated,
  unauthenticated,
  skipped,
}

class AuthRepository with ChangeNotifier {
  final AuthService service;
  final authDataSource = AuthDataSource(preferences);
  AuthRepository({required this.service});

  AuthState get state {
    if (_isSkipped) {
      return AuthState.skipped;
    }

    if (authDataSource.hasAuth) {
      return AuthState.authenticated;
    }

    return AuthState.unauthenticated;
  }

  User get currentUser => authDataSource.currentUser;

  bool _isSkipped = false;
  Future<void> skip() async {
    _isSkipped = true;
    notifyListeners();
  }

  Future<void> signIn({String? email, String? password}) async {
    final res = await service.signIn(
      email: email,
      password: password,
    );

    _isSkipped = false;
    await authDataSource.saveAuthResult(res);
    notifyListeners();
  }

  Future<bool> signUp({
    String? firstName,
    String? lastName,
    String? email,
    String? phone,
    String? password,
  }) {
    return service.signUp(
      email: email,
      firstName: firstName,
      lastName: lastName,
      password: password,
      phone: phone,
    );
  }

  Future<void> signOut() async {
    await authDataSource.clear();
    notifyListeners();
  }
}
