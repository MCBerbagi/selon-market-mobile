import 'package:flutter/foundation.dart';
import 'package:selon_market/selon_market.dart';

class CartRepository with ChangeNotifier {
  final CartService service;
  final authDataSource = AuthDataSource(preferences);

  CartRepository({required this.service}) {
    refresh();
  }

  List<Cart> _cartList = [];
  List<Cart> get cartList => _cartList;
  Future<void> refresh() async {
    _cartList = await service.fetchAll(userId: authDataSource.currentUserId);
    notifyListeners();
  }

  Future<bool> addToCart({
    required int qty,
    required Product product,
  }) async {
    final res = await service.addToCart(
      userId: authDataSource.currentUserId,
      qty: qty,
      product: product,
    );

    if (res) {
      refresh();
    }

    return res;
  }

  Future<void> delete(List<Cart> carts) async {
    for (var cart in carts) {
      await service.deleteCart(
          userId: authDataSource.currentUserId, cart: cart);
    }
    refresh();
  }
}
