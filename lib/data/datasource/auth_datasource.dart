import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:selon_market/selon_market.dart';

class AuthDataSource {
  final SharedPreferences preferences;
  AuthDataSource(this.preferences);

  bool get hasAuth => preferences.containsKey('auth');

  Future<void> saveAuthResult(AuthResult authResult) async {
    await preferences.setString('auth', authResult.authorization);
    await preferences.setString('user_id', authResult.userId);
    await preferences.setString('user', json.encode(authResult.user.toJson()));
  }

  Future<void> clear() async {
    await preferences.remove('auth');
    await preferences.remove('user');
  }

  User get currentUser {
    final str = preferences.getString('user')!;
    return User.fromJson(json.decode(str));
  }

  String get currentUserId => hasAuth ? preferences.getString('user_id')! : '';
  String get authorization => hasAuth ? preferences.getString('auth')! : '';
}
