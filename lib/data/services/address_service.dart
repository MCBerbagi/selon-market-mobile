import 'package:dio/dio.dart';
import 'package:selon_market/selon_market.dart';

class AddressService {
  final Dio dio;
  AddressService(this.dio);

  Future<List<ShippingAddress>> list({required String userId}) async {
    final res = await dio.get(
      'user/get/address',
      queryParameters: {
        'id': userId,
      },
    );

    if (res.statusCode == 200) {
      return (res.data['user']['address'] as List)
          .map((e) => ShippingAddress.fromJson(e))
          .toList();
    }
    return [];
  }

  Future<bool> add({
    required String userId,
    required ShippingAddress address,
  }) async {
    final data = address.toJson();
    data['userid'] = userId;
    data.remove('id');

    final res = await dio.post(
      'user/add/address',
      data: data,
      options: Options(
        headers: {
          'content': 'application/json',
          'Date': DateTime.now().toString(),
        },
      ),
    );

    return res.statusCode == 200;
  }

  Future<SearchResultList> search({required String district}) async {
    final res = await dio.get(
      'http://staging-delivery.kiselindonesia.net/location/search/$district',
      options: Options(
        headers: {
          'x-api-key': 'eyJzZXJ2aWNlIjoic2hpcHBlciIsImF1dGhvciI6InJ1cGlub3oifQ==',
        },
      ),
    );

    if (res.statusCode == 200) {
      return SearchResultList.fromJson(res.data);
    }
    return SearchResultList();
  }
}
