import 'package:dio/dio.dart';
import 'package:selon_market/selon_market.dart';

class CartService {
  final Dio dio;

  CartService(this.dio);

  Future<List<Cart>> fetchAll({required String userId}) async {
    final res = await dio.get(
      'cart/history/find/product',
      queryParameters: {
        'id': userId,
      },
    );

    if (res.statusCode == 200) {
      return (res.data['cart'] as List).map((e) => Cart.fromJson(e)).toList();
    }

    return [];
  }

  Future<bool> addToCart({
    required String userId,
    required int qty,
    required Product product,
  }) async {
    final res = await dio.post(
      'cart/history/add/product',
      data: {
        'userid': userId,
        'product_id': product.productId,
        'qty': qty,
        'product_json': product.toJson(),
      },
    );

    if (res.statusCode == 200 && res.data['status'] == 'OK') {
      return true;
    }

    return false;
  }

  Future<void> deleteCart({required String userId, required Cart cart}) async {
    await dio.delete(
      'cart/history/delete/product',
      queryParameters: {
        'id': userId,
        'order': cart.cartId,
      },
    );
  }
}
