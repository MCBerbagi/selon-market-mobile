import 'package:dio/dio.dart';
import 'package:selon_market/selon_market.dart';

class ProductService {
  final Dio dio;
  ProductService(this.dio);

  Future<List<Product>> fetchAll({int? size}) async {
    final res = await dio.get(
      'es-product/find/item/all?size=100',
      queryParameters: {
        if (size != null) 'size': size.toString(),
      },
    );

    if (res.statusCode == 200 && res.data['status'] == 'OK') {
      final List list = res.data['product_list'];
      return list.map((e) => Product.fromJson(e)).toList();
    }

    return [];
  }
}
