import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:selon_market/data/models/courier_new.dart';
import 'package:selon_market/selon_market.dart';

class DeliveryService {
  final dio = Dio();
  DeliveryService();

  Future<CourierNew> getCouriers({
    required int senderAreaId,
    required String senderCoordinate,
    required int receiverAreaId,
    required String receiverCoordinate,
    required int itemWeight,
    required int itemLength,
    required int itemWidth,
    required int itemHeight,
    required int itemPrice,
  }) async {
    try {
      final token = 'eyJzZXJ2aWNlIjoic2hpcHBlciIsImF1dGhvciI6InJ1cGlub3oifQ==';
      // final token = 'eyJzZXJ2aWNlIjoiZGVsaXZlcnkiLCJhdXRob3IiOiJydXBpbm96In0=';
      // final key = '$receiverZipCode$receiverCoordinate$token';
      // final digest = sha256.convert(utf8.encode(key));
      // final apiKey = digest.toString();

      final res = await dio.get(
        'http://staging-delivery.kiselindonesia.net/rates/domestic?sender_areaid=$senderAreaId&receiver_areaid=$receiverAreaId&item_weight=$itemWeight&item_length=$itemLength&item_width=$itemWidth&item_height=$itemHeight&item_price=$itemPrice&cod=0&type=2&order=0&sender_coord=$senderCoordinate&receiver_coord=$receiverCoordinate',

        options: Options(
          contentType: 'application/json',
          headers: {
            'x-api-key': token,
          },
          responseType: ResponseType.json,
          validateStatus: (code) => true,
        ),
      );

      if (res.statusCode == 200) {
        CourierNew _courier = CourierNew.fromJson(res.data);
        // final Map<String, dynamic> result = res.data['result'];
        // final list = <Courier>[
        //   for (var key in result.keys)
        //     if ((result[key] as String).contains(';'))
        //       Courier(
        //         name: key,
        //         services:
        //             (result[key] as String).split('|').map<CourierService>(
        //           (e) {
        //             final s = e.split(';');
        //             final x = s[0].split('=');
        //             return CourierService(
        //               name: x[0],
        //               price: double.parse(x[1]),
        //               description: s[1],
        //             );
        //           },
        //         ).toList(),
        //       ),
        // ];

        return _courier;
      }
    } catch (e) {
      print(e);
    }

    return CourierNew();
  }
}
