import 'package:dio/dio.dart';
import 'package:selon_market/selon_market.dart';

class AuthService {
  final Dio dio;
  AuthService(this.dio);

  Future<AuthResult> signIn({String? email, String? password}) async {
    final res = await dio.post(
      'auth/login',
      data: {
        "email": email,
        "password": password,
        "device": "android",
      },
    );

    if (res.statusCode == 200 && res.data['status'] == 'Ok') {
      return AuthResult.fromJson(res.data);
    }

    throw AuthenticationFailed();
  }

  Future<bool> signUp({
    String? firstName,
    String? lastName,
    String? email,
    String? phone,
    String? password,
  }) async {
    final res = await dio.post('user/registration', data: {
      "first_name": firstName,
      "last_name": lastName,
      "email": email,
      "phone": phone,
      "password": password,
    });

    return res.statusCode == 200 && res.data['status'] == 'Ok';
  }
}

class AuthenticationFailed implements Exception {}
