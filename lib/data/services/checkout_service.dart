import 'package:dio/dio.dart';
import 'package:selon_market/data/models/courier_new.dart';
import 'package:selon_market/selon_market.dart';

class OrderService {
  final Dio dio;
  OrderService(this.dio);

  Future<CheckoutResult?> checkout({
    required String userId,
    required Checkout checkout,
    required ShippingAddress receiver,
    // required Courier courier,
    // required CourierService courierService,
    required LogisticService logisticService,
    required String channelId,
  }) async {
    double totalAmount = checkout.items.fold<double>(
      0,
          (previousValue, element) =>
      previousValue += (element.qty * element.cart.price),
    );

    totalAmount += logisticService.finalRate!;

    final data = <String, dynamic>{
      'userid': userId,
      'receiver': receiver.toJson(),
      'sender': checkout.outlet.toJson(),
      'product_ordered': [
        for (var item in checkout.items)
          {
            'product_id': item.cart.productId,
            'qty': item.qty,
            'price': (item.cart.price * item.qty).toInt(),
          }
      ],
      'provider_name': logisticService.name,
      'provider_service': (logisticService.showId == 1)
          ? "Regular"
          : (logisticService.showId == 9)
          ? "Instant"
          : (logisticService.showId == 8)
          ? "Same day"
          : "Express",
      'provider_price': logisticService.finalRate!.toInt(),
      'channel_id': channelId,
      'total_amount': totalAmount.toInt(),
    };

    final res = await dio.post(
      'checkout/payment/product',
      data: data,
    );

    if (res.statusCode == 200) {
      return CheckoutResult.fromJson(res.data['order']);
    }

    return null;
  }

  Future<List<Order>> orders({required String userId}) async {
    final res = await dio.get('checkout/order/history', queryParameters: {
      'id': userId,
    });

    if (res.statusCode == 200) {
      return (res.data['checkout_history'] as List)
          .map((e) => Order.fromJson(e))
          .toList();
    }

    return [];
  }
}
