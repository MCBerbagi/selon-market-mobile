import 'package:dio/dio.dart';
import 'package:selon_market/selon_market.dart';

class PromoService {
  final Dio dio;
  PromoService(this.dio);

  Future<List<BannerItem>> getBanners({required String userId}) async {
    final res = await dio.get(
      'promo/list/banner',
      queryParameters: {
        'id': userId,
      },
    );

    if (res.statusCode == 200 && res.data['status'] == 'OK') {
      return (res.data['banners'] as List)
          .map((e) => BannerItem.fromJson(e))
          .toList();
    }

    return [];
  }
}
