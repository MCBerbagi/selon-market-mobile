import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:selon_market/selon_market.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initializePreference();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent,
    ));

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => ConfigRepository(),
        ),
        ChangeNotifierProvider(
          create: (context) => AuthRepository(
            service: AuthService(dio),
          ),
        ),
      ],
      builder: (context, child) => MaterialApp(
        title: "Selon Market",
        theme: ThemeData(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          primaryColorLight: Colors.white,
          primaryColorBrightness: Brightness.light,
          primaryColor: Colors.white,
          cardTheme: CardTheme(
            clipBehavior: Clip.antiAlias,
            elevation: 8.0,
            shadowColor: Colors.black.withOpacity(0.5),
          ),
        ),
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          Locale('en', ''),
          Locale('zh', ''),
          Locale('ar', ''),
          Locale('hi', ''),
          Locale('id', '')
        ],
        locale: context.watch<ConfigRepository>().locale,
        routes: routes,
        builder: (context, navigator) {
          final ConfigRepository configRepository = context.watch();
          if (configRepository.showSplashScreen) {
            return Navigator(
              pages: [
                MaterialPage(
                  key: ValueKey('splash'),
                  child: SplashScreen(),
                ),
              ],
              onPopPage: (route, result) => route.didPop(result),
            );
          }

          if (configRepository.showOnBoarding) {
            return Navigator(
              pages: [
                MaterialPage(
                  key: ValueKey('on_boarding'),
                  child: OnBoardingScreen(),
                ),
              ],
              onPopPage: (route, result) => route.didPop(result),
            );
          }

          final AuthRepository authRepository = context.watch();
          if (authRepository.state == AuthState.unauthenticated) {
            return Navigator(
              pages: [
                MaterialPage(
                  key: ValueKey('landing'),
                  child: ChooseLogin(),
                ),
              ],
              onPopPage: (route, result) => route.didPop(result),
            );
          }


          return MultiProvider(
            providers: [
              ChangeNotifierProvider(
                create: (context) => ProfileRepository(),
              ),
              ChangeNotifierProvider(
                create: (context) => ProductRepository(
                  service: ProductService(dio),
                ),
              ),
              ChangeNotifierProvider(
                lazy: false,
                create: (context) => OrderRepository(
                  service: OrderService(dio),
                ),
              ),
              ChangeNotifierProvider(
                create: (context) => PromoRepository(
                  service: PromoService(dio),
                ),
              ),
              ChangeNotifierProvider(
                create: (context) => AddressRepository(
                  service: AddressService(dio),
                ),
              ),
              ChangeNotifierProvider(
                lazy: false,
                create: (context) => CartRepository(
                  service: CartService(dio),
                ),
              ),
            ],
            child: navigator,
          );
        },
      ),
    );
  }
}
