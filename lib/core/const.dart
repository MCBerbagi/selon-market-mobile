import 'package:selon_market/data/models/list_hint_payment.dart';
import 'package:selon_market/selon_market.dart';

// TODO : ganti icon bank
const availablePaymentMethods = <PaymentMethod>[
  const PaymentMethod(
    bank: 'Bank BCA',
    description: 'Bank BCA Virtual Account',
    channelId: 'KUTBVA01',
    admin: 3000,
    logo: 'assets/img/bca.png',
    listHintPayment: [
      HintPaymentData(hintText: "ATM", listHint: hintAtmBCA),
      HintPaymentData(
          hintText: "Mobile Banking", listHint: hintMobileBankingBCA)
    ],
  ),
  const PaymentMethod(
    bank: 'Bank BRI',
    description: 'Bank BRI Virtual Account (BRIVA)',
    channelId: 'KUTBRIVA01',
    admin: 4000,
    logo: 'assets/img/bri.png',
    listHintPayment: [
      HintPaymentData(hintText: "ATM", listHint: hintAtmBRI),
      HintPaymentData(
          hintText: "Mobile Banking", listHint: hintMobileBankingBRI)
    ],
  ),
  const PaymentMethod(
    bank: 'Bank BNI',
    description: 'Bank BNI Virtual Account',
    channelId: 'KUTBNIVA01',
    admin: 4000,
    logo: 'assets/img/bni.png',
    listHintPayment: [
      HintPaymentData(hintText: "ATM", listHint: hintAtmBNI),
      HintPaymentData(hintText: "Mobile Banking", listHint: hintMobileBankingBNI)
    ],
  ),
  const PaymentMethod(
    bank: 'Bank Mandiri',
    description: 'Bank Mandiri Virtual Account',
    channelId: 'KUTMVA01',
    admin: 4000,
    logo: 'assets/img/mandiri.png',
    listHintPayment: [
      HintPaymentData(hintText: "ATM", listHint: hintAtmMandiri),
      HintPaymentData(hintText: "Mobile Banking", listHint: hintMobileBankingMandiri)
    ],
  ),
];

const hintAtmBNI = [
  "Masukkan Kartu Anda.",
  "Pilih Bahasa.",
  "Masukkan PIN ATM Anda.",
  "Kemudian, pilih Menu Lainnya.",
  'Pilih Transfer dan pilih Jenis rekening yang akan Anda gunakan (Contoh: "Dari Rekening Tabungan").',
  "Pilih Virtual Account Billing. Masukkan nomor Virtual Account Anda (Contoh: 8277087781881441).",
  "Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.",
  "Konfirmasi, apabila telah sesuai, lanjutkan transaksi.",
  "Transaksi Anda telah selesai.",
];

const hintMobileBankingBNI = [
  "Akses BNI Mobile Banking melalui handphone.",
  "Masukkan User ID dan password.",
  "Pilih menu Transfer.",
  "Pilih menu Virtual Account Billing, lalu pilih rekening debet.",
  "Masukkan nomor Virtual Account Anda (Contoh: 8277087781881441) pada menu Input Baru.",
  "Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.",
  "Konfirmasi transaksi dan masukkan Password Transaksi.",
  "Pembayaran Anda Telah Berhasil.",
];

const hintAtmBRI = [
  "Pilih “Transaksi Lain”",
  "Pilih “Menu Lainnya”",
  "Pilih “Pembayaran”",
  "Pilih “Menu Lainnya”",
  "Pilih “Briva”",
  "Masukkan Nomor BRI Virtual Account (Contoh: 26215xxxxxxxxxxxx), lalu tekan “Benar”",
  "Konfirmasi pembayaran, tekan “Ya” bila sesuai",
];

const hintMobileBankingBRI = [
  "Log in ke Mobile Banking",
  "Pilih “Pembayaran”",
  "Pilih “BRIVA”",
  "Masukkan nomor BRI Virtual Account dan jumlah pembayaran",
  "Masukkan nomor PIN anda",
  "Tekan “OK” untuk melanjutkan transaksi",
  "Transaksi berhasil",
  "SMS konfirmasi akan masuk ke nomor telepon anda",
];

const hintAtmBCA = [
  "Masukkan Kartu ATM BCA & PIN.",
  "Pilih menu Transaksi Lainnya > Transfer > ke Rekening BCA Virtual Account.",
  "Masukkan nomor Virtual Account Anda (Contoh: 8277087781881441).",
  "Di halaman konfirmasi, pastikan detil pembayaran sudah sesuai seperti No VA, Nama, Perus/Produk dan Total Tagihan.",
  "Pastikan nama kamu dan Total Tagihannya benar.",
  "Jika sudah benar, klik Ya.",
  "Simpan struk transaksi sebagai bukti pembayaran.",
];

const hintMobileBankingBCA = [
  "Lakukan log in pada aplikasi BCA Mobile.",
  "Pilih menu m-BCA, kemudian masukkan kode akses m-BCA.",
  "Pilih m-Transfer > BCA Virtual Account.",
  "Pilih dari Daftar Transfer, atau masukkan nomor Virtual Account Anda (Contoh: 8277087781881441).",
  "Pastikan nama kamu dan Total Tagihannya sudah benar.",
  "Kemudian klik Ok dan masukkan pin m-BCA.",
  "Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran.",
];

const hintAtmMandiri = [
  "Silahkan pilih Bayar/Beli",
  "Kemudian pilih Lainnya > Lainnya > Multi Payment",
  "Masukkan kode bayar dengan nomor Virtual Account Anda (contoh: 7810202001539202) dan klik Benar",
  "Jangan lupa untuk memeriksa informasi yang tertera pada layar. Jika benar, tekan angka 1 dan pilih Ya",
  "Periksa layar konfirmasi dan pilih Ya"
];

const hintMobileBankingMandiri = [
  "Silahkan login ke mBanking Anda. Kemudian pilih Bayar. Atau apabila Anda menggunakan bahasa inggris, silahkan pilih Payment.",
  "Kemudian klik Multipayment",
  "Silakan klik Penyedia Jasa.",
  "Kemudian pilih Transferpay",
  "Masukkan kode bayar dengan Virtual Account Number Anda (contoh: 7810202001539202), kemudian pilih Lanjut",
  "Jangan lupa untuk memeriksa informasi yang tertera di layar! Jika sudah benar, masukkan PIN Anda dan pilih OK."
];
