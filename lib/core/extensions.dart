import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';

extension BuildContextExt on BuildContext {
  AppLocalizations get l10n => AppLocalizations.of(this)!;
}

extension NumExt on num {
  String format({String format = '#,##0', String? locale}) {
    final formatter = NumberFormat(format, locale ?? 'eu');
    return formatter.format(this);
  }
}

extension DateExt on DateTime {
  String format() {
    final formatter = DateFormat('EEEE, dd MMMM yyyy HH:mm');
    return formatter.format(this);
  }
}
