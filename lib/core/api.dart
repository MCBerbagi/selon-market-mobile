class Enva {
  String baseURL = "https://api.selonmarket.com/api/v1/";

  Uri postLoginCustomer() {
    return Uri.parse(baseURL + "auth/login");
  }

  Uri postLogoutCustomer() {
    return Uri.parse(baseURL + "user/logout");
  }

  Uri postRegisterCustomer() {
    return Uri.parse(baseURL + "user/registration");
  }

  Uri postProfileCustomer() {
    return Uri.parse(baseURL + "/user/profile/update/");
  }

  Uri postsmsotp() {
    return Uri.parse(baseURL + "user/phone/login/");
  }

  Uri postgenerateotp() {
    return Uri.parse(baseURL + "user/otp/generate/");
  }

  Uri postloginotp() {
    return Uri.parse(baseURL + "user/user/phone/login/");
  }

  Uri getProvinsi() {
    return Uri.parse(baseURL + "/register/province");
  }

  Uri getKota(int provId) {
    return Uri.parse(baseURL + "/register/city/" + provId.toString());
  }

  Uri getKecamatan(int cityId) {
    return Uri.parse(baseURL + "/register/district/" + cityId.toString());
  }

  Uri getproductfecthall(String size) {
    return Uri.parse(baseURL + "es-product/find/item/all?size=" + size);
  }

  Uri getcartall(String userID) {
    return Uri.parse(baseURL + "cart/history/find/product?id=" + userID);
  }

  Uri getbannerall(String userID) {
    return Uri.parse(baseURL + "promo/list/banner?id=" + userID);
  }

  Uri getaddress(String userID) {
    return Uri.parse(baseURL + "api/v1/user/get/address?id=" + userID);
  }

  Uri postcartall() {
    return Uri.parse(baseURL + "cart/history/add/product");
  }

  Uri editcartall() {
    return Uri.parse(baseURL + "cart/history/update/product");
  }

  Uri deletecartall(String userID, String orderID) {
    return Uri.parse(baseURL +
        "cart/history/delete/product?id=" +
        userID +
        "&order=" +
        orderID);
  }

  Uri getproductdetailname(String size, String nama) {
    return Uri.parse(
        baseURL + "es-product/find/item?search=" + nama + "&size=" + size);
  }

  Uri getproductname(String id) {
    return Uri.parse(baseURL + "es-product/detail/item?id=" + id);
  }

  Uri productlistall() {
    return Uri.parse(baseURL + "es-product/detail/item");
  }

  Uri addcart() {
    return Uri.parse(baseURL + "cart/history/add/product");
  }

  Uri listcart() {
    return Uri.parse(baseURL + "cart/history/find/product");
  }

  Uri deletecart() {
    return Uri.parse(baseURL + "cart/history/delete/product");
  }

  Uri updatecart() {
    return Uri.parse(baseURL + "cart/history/update/product");
  }
}
