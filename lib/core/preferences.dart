import 'package:shared_preferences/shared_preferences.dart';

late SharedPreferences _preferences;
SharedPreferences get preferences => _preferences;

Future<void> initializePreference() async {
  _preferences = await SharedPreferences.getInstance();
}
