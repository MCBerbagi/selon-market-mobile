import 'package:dio/dio.dart';
import 'package:selon_market/selon_market.dart';

final dio = Dio(
  BaseOptions(
    baseUrl: 'https://api.selonmarket.com/api/v1/',
    responseType: ResponseType.json,
    contentType: 'application/json',
    validateStatus: (code) => code! < 500,
  ),
)..interceptors.add(
    InterceptorsWrapper(
      onRequest: (options, handler) {
        final authDataSource = AuthDataSource(preferences);
        if (authDataSource.hasAuth) {
          options.headers['client-key'] = authDataSource.authorization;
        }

        handler.next(options);
      },
    ),
  );
