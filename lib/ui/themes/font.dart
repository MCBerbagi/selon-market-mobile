import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:selon_market/selon_market.dart';

class ThemeFonts {
  static double headerFontSize = 18.0;
  static double defaultFontSize = 14.0;

  static TextStyle textStyle100 = GoogleFonts.assistant(
      fontSize: defaultFontSize,
      fontWeight: FontWeight.w100,
      color: ThemeColors.primaryTextColor);

  static TextStyle textStyle200 = GoogleFonts.assistant(
      fontSize: defaultFontSize,
      fontWeight: FontWeight.w200,
      color: ThemeColors.primaryTextColor);

  static TextStyle textStyle300 = GoogleFonts.assistant(
      fontSize: defaultFontSize,
      fontWeight: FontWeight.w300,
      color: ThemeColors.primaryTextColor);

  static TextStyle textStyle400 = GoogleFonts.assistant(
    fontSize: defaultFontSize,
    fontWeight: FontWeight.w400,
    color: ThemeColors.primaryTextColor,
  );

  static TextStyle textStyle500 = GoogleFonts.assistant(
      fontSize: defaultFontSize,
      fontWeight: FontWeight.w500,
      color: ThemeColors.primaryTextColor);

  static TextStyle textStyle600 = GoogleFonts.assistant(
      fontSize: defaultFontSize,
      fontWeight: FontWeight.w600,
      color: ThemeColors.primaryTextColor);

  static TextStyle textStyle700 = GoogleFonts.assistant(
      fontSize: defaultFontSize,
      fontWeight: FontWeight.w700,
      color: ThemeColors.primaryTextColor);

  static TextStyle textStyle800 = GoogleFonts.assistant(
      fontSize: defaultFontSize,
      fontWeight: FontWeight.w800,
      color: ThemeColors.primaryTextColor);

  static TextStyle textStyle900 = GoogleFonts.assistant(
      fontSize: defaultFontSize,
      fontWeight: FontWeight.w900,
      color: ThemeColors.primaryTextColor);
}
