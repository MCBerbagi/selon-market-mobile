import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

const Duration _kExpand = Duration(milliseconds: 200);

class ExpansionTileReview extends StatefulWidget {
  final Widget? leading;

  final Widget title;

  final ValueChanged<bool>? onExpansionChanged;

  final List<Widget> children;

  final Color? backgroundColor;

  final Widget? trailing;

  final bool initiallyExpanded;

  const ExpansionTileReview({
    Key? key,
    this.leading,
    required this.title,
    this.backgroundColor,
    this.onExpansionChanged,
    this.children = const <Widget>[],
    this.trailing,
    this.initiallyExpanded = false,
  }) : super(key: key);

  @override
  _ExpansionTileReviewState createState() => _ExpansionTileReviewState();
}

class _ExpansionTileReviewState extends State<ExpansionTileReview>
    with SingleTickerProviderStateMixin {
  static var _detailText = TextStyle(
      fontFamily: "Gotik",
      color: Colors.black54,
      fontWeight: FontWeight.w600,
      letterSpacing: 0.3,
      wordSpacing: 0.5);

  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn);

  final ColorTween _borderColorTween = ColorTween();
  final ColorTween _headerColorTween = ColorTween();
  final ColorTween _iconColorTween = ColorTween();
  final ColorTween _backgroundColorTween = ColorTween();

  late AnimationController _controller;
  late Animation<double> _heightFactor;
  late Animation<Color?> _iconColor;

  bool _isExpanded = false;
  bool _gradientExpanded = true;
  String _readMore = "...read more";
  @override
  Widget build(BuildContext context) {
    final bool closed = !_isExpanded && _controller.isDismissed;
    return AnimatedBuilder(
        animation: _controller.view,
        builder: _buildChildren,
        child: closed
            ? null
            : Container(
                width: double.infinity,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: widget.children),
              ));
  }

  @override
  void didChangeDependencies() {
    final ThemeData theme = Theme.of(context);
    _borderColorTween..end = theme.dividerColor;
    _headerColorTween
      ..begin = theme.textTheme.subtitle1!.color
      ..end = theme.accentColor;
    _iconColorTween
      ..begin = theme.unselectedWidgetColor
      ..end = theme.accentColor;
    _backgroundColorTween..end = widget.backgroundColor;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: _kExpand, vsync: this);
    _heightFactor = _controller.drive(_easeInTween);
    _iconColor = _controller.drive(_iconColorTween.chain(_easeInTween));
    _gradientExpanded = true;
    _isExpanded =
        PageStorage.of(context)?.readState(context) ?? widget.initiallyExpanded;
    if (_isExpanded) _controller.value = 1.0;
  }

  Widget _buildChildren(BuildContext context, Widget? child) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IconTheme.merge(
              data: IconThemeData(color: _iconColor.value),
              child: GestureDetector(
                onTap: _handleTap,
                child: Stack(
                  children: <Widget>[
                    widget.title,
                    _gradientExpanded
                        ? Container(
                            width: double.infinity,
                            decoration: BoxDecoration(),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 18.0, left: 166.0),
                              child: Container(
                                  padding: EdgeInsets.only(left: 4.0),
                                  color: Colors.white,
                                  alignment: Alignment.bottomLeft,
                                  child: Text(_readMore, style: _detailText)),
                            ),
                          )
                        : Container()
                  ],
                ),
              )),
          ClipRect(
            child: Align(
              heightFactor: _heightFactor.value,
              child: child,
            ),
          ),
        ],
      ),
    );
  }

  void _handleTap() {
    setState(() {
      _isExpanded = !_isExpanded;
      if (_isExpanded) {
        _controller.forward();
        setState(() {
          _gradientExpanded = false;
        });
      } else {
        _controller.reverse().then<void>((void value) {
          if (!mounted) return;
          setState(() {
            _gradientExpanded = true;
          });
        });
      }
      if (_readMore == "...read more") {
        setState(() {
          _readMore = " ";
        });
      } else if (_readMore == " ") {
        setState(() {
          _readMore = "...read more";
        });
      }
      PageStorage.of(context)?.writeState(context, _isExpanded);
    });
    if (widget.onExpansionChanged != null)
      widget.onExpansionChanged!(_isExpanded);
  }
}
