import 'package:flutter/widgets.dart';
import 'package:selon_market/selon_market.dart';
import 'package:selon_market/ui/screen/checkout/checkout_screen.dart';
import 'package:selon_market/ui/screen/location_picker/location_picker.dart';
import 'package:selon_market/ui/screen/order/widgets/invoice.dart';

Map<String, WidgetBuilder> get routes => {
      '/': (context) => MainScreen(),
      '/product': (context) => DetailProdukScreen(
            item: ModalRoute.of(context)!.settings.arguments as Product,
          ),
      '/private-message': (context) => PrivateMessage(
            brand: ModalRoute.of(context)!.settings.arguments as Brand,
          ),
      '/ppob': (context) => MenuDetailScreen(categoryName: ModalRoute.of(context)!.settings.arguments as String,),
      '/food': (context) => MenuDetailScreen(categoryName: ModalRoute.of(context)!.settings.arguments as String,),
      '/selebriti': (context) => MenuDetailScreen(categoryName: ModalRoute.of(context)!.settings.arguments as String,),
      '/elektronik': (context) => MenuDetailScreen(categoryName: ModalRoute.of(context)!.settings.arguments as String,),
      '/search': (context) => SearchScreen(categoryName: ModalRoute.of(context)!.settings.arguments as String,),
      '/fashion': (context) => CategoryDetail(categoryName: ModalRoute.of(context)!.settings.arguments as String,),
      '/cheff': (context) => CategoryDetail(categoryName: ModalRoute.of(context)!.settings.arguments as String,),
      '/notification': (context) => NotificationScreen(),
      '/delivery': (context) => DeliveryScreen(),
      '/payment': (context) => PaymentScreen(),
      '/message': (context) => ChatScreen(),
      '/orders': (context) => OrderPage(),
      '/order-detail': (context) => OrderScreen(),
      '/account': (context) => AccountScreen(),
      '/call-center': (context) => CallCenterScreen(),
      '/language': (context) => LanguageSettingScreen(),
      '/about': (context) => AboutAppScreen(),
      '/chat': (context) => ChatScreen(),
      '/promo': (context) => PromoDetailScreen(categoryName: ModalRoute.of(context)!.settings.arguments as String,),
      '/cart': (context) => CartScreen(),
      '/reviews': (context) => ReviewsAllScreen(),
      '/product-chat': (context) => ChatItemScreen(),
      '/call-center-chat': (context) => ChatItemScreen(),
      '/credit-card': (context) => CreditCardSetting(),
      '/flash-sale': (context) => FlashSaleScreen(),
      '/flash-sale-detail': (context) => FlashSaleDetailScreen(
            itemSale: ModalRoute.of(context)!.settings.arguments as SaleItem,
          ),
      '/brand': (context) => BrandDetailScreen(
            brand: ModalRoute.of(context)!.settings.arguments as Brand,
          ),
      '/pick-location': (context) => LocationPicker(),
      '/address': (context) => AddressScreen(
            isSelector: false,
          ),
      '/pick-address': (context) => AddressScreen(
            isSelector: true,
          ),
      '/checkout': (context) => CheckoutScreen(),
      '/virtual-account': (context) => VirtualAccountScreen(),
      '/invoice': (context) => Invoice(),
    };
