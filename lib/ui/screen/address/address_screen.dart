import 'package:flutter/material.dart';
import 'package:selon_market/selon_market.dart';

class AddressScreen extends StatefulWidget {
  final bool isSelector;
  AddressScreen({
    Key? key,
    required this.isSelector,
  }) : super(key: key);

  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Color(0xFFFF0000)),
        centerTitle: true,
        backgroundColor: Colors.red,
        title: Text(
          'Address',
          style: TextStyle(
              fontFamily: "Gotik",
              fontSize: 18.0,
              color: Colors.white,
              fontWeight: FontWeight.w700),
        ),
        elevation: 0.0,
      ),
      body: buildBody(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.red,
        onPressed: () async {
          final res = await Navigator.pushNamed(context, '/pick-location');
          if (res != null) {
            final address = res as ShippingAddress;
            AddressRepository repository = context.read();
            // repository.add(address: address);
            repository.searchByAddress(address: address);
          }
        },
      ),
    );
  }

  Widget buildBody() {
    final repository = context.watch<AddressRepository>();
    final list = repository.addressList;
    return RefreshIndicator(
      onRefresh: repository.refresh,
      child: ListView.builder(
        physics: AlwaysScrollableScrollPhysics(),
        padding: const EdgeInsets.all(8.0),
        itemBuilder: (context, index) {
          final item = list.elementAt(index);
          return Card(
            child: ListTile(
              dense: true,
              title: Text(item.city),
              subtitle: Text(item.address),
              onTap: widget.isSelector
                  ? () {
                Navigator.pop(context, item);
              }
                  : null,
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
