import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:selon_market/selon_market.dart';
import 'package:selon_market/ui/screen/address/address_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int currentIndex = 0;

  Widget callPage(int current) {
    switch (current) {
      case 1:
        return AddressScreen(
          isSelector: false,
        );

      case 2:
        return CartScreen();

      case 3:
        return ProfileScreen();

      default:
        return HomeScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion(
      value: SystemUiOverlayStyle(
        statusBarBrightness: Brightness.dark,
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
        body: callPage(currentIndex),
        bottomNavigationBar: Theme(
            data: Theme.of(context).copyWith(
                canvasColor: Colors.white,
                textTheme: Theme.of(context)
                    .textTheme
                    .copyWith(caption: TextStyle(color: Colors.black12))),
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              currentIndex: currentIndex,
              fixedColor: Color(0xFFFF0000),
              unselectedItemColor: Colors.black12,
              onTap: (value) {
                setState(() {
                  currentIndex = value;
                });
              },
              items: [
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: context.l10n.kHome,
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.add_location),
                  label: context.l10n.kBrand,
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.shopping_cart),
                  label: context.l10n.kCart,
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                  label: context.l10n.kAccount,
                ),
              ],
            )),
      ),
    );
  }
}
