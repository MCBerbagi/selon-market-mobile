import 'dart:async';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:selon_market/selon_market.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen>
    with TickerProviderStateMixin {
  //Animation Declaration
  late AnimationController sanimationController;
  AnimationController? animationControllerScreen;
  Animation? animationScreen;
  String deviceIdAndroid = "";
  String deviceIdIos = "";
  bool password = true;
  bool password1 = true;
  final List<String?> errors = [];

  void _toggle() {
    setState(() {
      password = !password;
    });
  }

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void getDeviceinfoAndroid() async {
    androidDeviceInfo =
        await deviceInfo.androidInfo; // instantiate Android Device Information
    setState(() {
      deviceIdAndroid = androidDeviceInfo.androidId;
    });
  }

  //   setState(() {
  //     deviceId = deviceData;
  //   });
  // }
  void getDeviceinfoIos() async {
    iosDeviceInfo =
        await deviceInfo.iosInfo; // instantiate Ios Device Information
    setState(() {
      deviceIdIos = iosDeviceInfo.identifierForVendor;
    });
  }

  //dipake
  TextEditingController emailController = TextEditingController();
  TextEditingController firstnameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController noHandphoneController = TextEditingController();

  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  late AndroidDeviceInfo androidDeviceInfo;
  late IosDeviceInfo iosDeviceInfo;

  Future clearAllFeild() async {
    setState(() {
      //dipake
      emailController.clear();
      firstnameController.clear();
      lastnameController.clear();
      passwordController.clear();
      confirmPasswordController.clear();
      noHandphoneController.clear();
    });
  }

  var tap = 0;

  /// Set AnimationController to initState
  @override
  void initState() {
    super.initState();
    sanimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 800))
          ..addStatusListener((statuss) {
            if (statuss == AnimationStatus.dismissed) {
              setState(() {
                tap = 0;
              });
            }
          });
  }

  /// Dispose animationController
  @override
  void dispose() {
    sanimationController.dispose();
    super.dispose();
  }

  /// Component Widget layout UI
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            /// Set Background image in layout
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage("assets/img/backgroundgirl.png"),
              fit: BoxFit.cover,
            )),
            child: Container(
              /// Set gradient color in image
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(0, 0, 0, 0.2),
                    Color.fromRGBO(0, 0, 0, 0.3)
                  ],
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                ),
              ),

              /// Set component layout
              child: ListView(
                padding: EdgeInsets.all(0.0),
                children: <Widget>[
                  Stack(
                    alignment: AlignmentDirectional.bottomCenter,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            alignment: AlignmentDirectional.topCenter,
                            child: Column(
                              children: <Widget>[
                                /// padding logo
                                Padding(
                                    padding: EdgeInsets.only(
                                        top:
                                            mediaQueryData.padding.top + 20.0)),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage("assets/img/Logo.png"),
                                      height: 70.0,
                                    ),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0)),

                                    /// Animation text treva shop accept from login layout
                                    Hero(
                                      tag: "Selon Market",
                                      child: Text(
                                        context.l10n.kTitle,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w900,
                                            letterSpacing: 0.6,
                                            fontFamily: "Sans",
                                            color: Colors.white,
                                            fontSize: 20.0),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 20.0)),

                                /// TextFromField Email
                                fieldEmail(),
                                Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 5.0)),

                                /// TextFromField Password
                                fieldFirstname(),
                                Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 5.0)),

                                /// TextFromField Password
                                fieldLastname(),
                                Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 5.0)),

                                /// TextFromField Password
                                fieldnoHP(),
                                Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 5.0)),

                                /// TextFromField Password
                                fieldPassword(),
                                Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 5.0)),

                                /// TextFromField Password
                                fieldConfirmPassword(),

                                /// Button Login
                                TextButton(
                                    style: TextButton.styleFrom(
                                      padding: EdgeInsets.only(top: 20.0),
                                    ),
                                    onPressed: () {
                                      ChooseLogin.of(context)!.show = 1;
                                    },
                                    child: Text(
                                      context.l10n.kNotHaveLogin,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 13.0,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: "Sans"),
                                    )),
                                Padding(
                                  padding: EdgeInsets.only(
                                      top: mediaQueryData.padding.top + 50.0,
                                      bottom: 0.0),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),

                      /// Set Animaion after user click buttonLogin
                      buttonLogin(context),
                      // buttonKembali(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// textfromfieldUsername custom class
  Widget fieldFirstname() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25.0),
      child: TextField(
        controller: firstnameController,
        keyboardType: TextInputType.text,
        style:
            ThemeFonts.textStyle500.copyWith(fontSize: 15, color: Colors.black),
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey[300],
          labelText: "Nama Depan",
          labelStyle: ThemeFonts.textStyle500
              .copyWith(fontSize: 18, color: Colors.grey),
          border: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          enabledBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
        ),
      ),
    );
  }

  Widget fieldLastname() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25.0),
      child: TextField(
        controller: lastnameController,
        keyboardType: TextInputType.text,
        style:
            ThemeFonts.textStyle500.copyWith(fontSize: 15, color: Colors.black),
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey[300],
          labelText: "Nama Belakang",
          labelStyle: ThemeFonts.textStyle500
              .copyWith(fontSize: 18, color: Colors.grey),
          border: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          enabledBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
        ),
      ),
    );
  }

  Widget fieldEmail() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25.0),
      child: TextField(
        controller: emailController,
        keyboardType: TextInputType.text,
        style:
            ThemeFonts.textStyle500.copyWith(fontSize: 15, color: Colors.black),
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey[300],
          labelText: "Email",
          labelStyle: ThemeFonts.textStyle500
              .copyWith(fontSize: 18, color: Colors.grey),
          border: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          enabledBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
        ),
      ),
    );
  }

  Widget fieldnoHP() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25.0),
      child: TextField(
        controller: noHandphoneController,
        keyboardType: TextInputType.number,
        style:
            ThemeFonts.textStyle500.copyWith(fontSize: 15, color: Colors.black),
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey[300],
          labelText: "Nomor Handphone",
          labelStyle: ThemeFonts.textStyle500
              .copyWith(fontSize: 18, color: Colors.grey),
          border: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          enabledBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
        ),
      ),
    );
  }

  /// textfromfieldPassword custom class
  Widget fieldPassword() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25.0),
      child: TextField(
        controller: passwordController,
        style:
            ThemeFonts.textStyle500.copyWith(fontSize: 15, color: Colors.black),
        obscureText: password,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey[300],
          suffixIcon: IconButton(
            color: Colors.black,
            onPressed: () {
              setState(() {
                _toggle();
              });
            },
            icon: password
                ? Icon(MdiIcons.eyeOffOutline)
                : Icon(MdiIcons.eyeOutline),
          ),
          labelText: "Kata Sandi",
          labelStyle: ThemeFonts.textStyle500
              .copyWith(fontSize: 18, color: Colors.grey),
          border: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          enabledBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
        ),
      ),
    );
  }

  /// textfromfieldPassword custom class
  Widget fieldConfirmPassword() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25.0),
      child: TextField(
        controller: confirmPasswordController,
        style:
            ThemeFonts.textStyle500.copyWith(fontSize: 15, color: Colors.black),
        obscureText: password,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey[300],
          suffixIcon: IconButton(
            color: Colors.black,
            onPressed: () {
              setState(() {
                _toggle();
              });
            },
            icon: password
                ? Icon(MdiIcons.eyeOffOutline)
                : Icon(MdiIcons.eyeOutline),
          ),
          labelText: "Kata Sandi",
          labelStyle: ThemeFonts.textStyle500
              .copyWith(fontSize: 18, color: Colors.grey),
          border: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          enabledBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
        ),
      ),
    );
  }

  Widget buttonLogin(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25.0),
      child: TextButton(
        onPressed: () {
          setState(() {
            if (passwordController.text != confirmPasswordController.text) {
              showDialog(
                  context: context,
                  builder: (BuildContext builderContext) {
                    Future.delayed(Duration(seconds: 2), () {
                      // Navigator.of(context).pop();
                    });
                    return AlertDialog(
                      backgroundColor: Colors.white,
                      title: Center(
                        child: Text(
                          "password tidak sama",
                          style: ThemeFonts.textStyle400
                              .copyWith(fontSize: 17, color: Colors.black),
                        ),
                      ),
                      // content: SingleChildScrollView(
                      //   child: Text('Content'),
                      // ),
                    );
                  });
            } else if (firstnameController.text == "" ||
                lastnameController.text == "" ||
                emailController.text == "" ||
                confirmPasswordController.text == "" ||
                noHandphoneController.text == "") {
              showDialog(
                  context: context,
                  builder: (BuildContext builderContext) {
                    Future.delayed(Duration(seconds: 1), () {
                      // Navigator.of(context).pop();
                    });
                    return AlertDialog(
                      backgroundColor: Colors.white,
                      title: Center(
                        child: Text(
                          "ada kolom yang kosong",
                          style: ThemeFonts.textStyle400
                              .copyWith(fontSize: 17, color: Colors.black),
                        ),
                      ),
                      // content: SingleChildScrollView(
                      //   child: Text('Content'),
                      // ),
                    );
                  });
            } else {
              done();
            }
          });
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.all(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
        ),
        child: Ink(
          decoration: BoxDecoration(
              boxShadow: [BoxShadow(color: Colors.black38, blurRadius: 15.0)],
              borderRadius: BorderRadius.circular(30.0),
              gradient: LinearGradient(
                  colors: <Color>[Color(0xFFFF0000), Color(0xFF790001)])),
          child: Container(
            alignment: Alignment.center,
            constraints:
                BoxConstraints(maxWidth: double.infinity, minHeight: 50),
            child: Text(
              context.l10n.kSignUp,
              style: TextStyle(
                  color: Colors.white,
                  letterSpacing: 0.2,
                  fontFamily: "Sans",
                  fontSize: 18.0,
                  fontWeight: FontWeight.w800),
            ),
          ),
        ),
      ),
    );
  }


  //tombol kembali ke login
  Widget buttonKembali(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25.0),
      child: TextButton(
        onPressed: () {
          setState(() {
            if (passwordController.text != confirmPasswordController.text) {
              showDialog(
                  context: context,
                  builder: (BuildContext builderContext) {
                    Future.delayed(Duration(seconds: 2), () {
                      // Navigator.of(context).pop();
                    });
                    return AlertDialog(
                      backgroundColor: Colors.white,
                      title: Center(
                        child: Text(
                          "password tidak sama",
                          style: ThemeFonts.textStyle400
                              .copyWith(fontSize: 17, color: Colors.black),
                        ),
                      ),
                      // content: SingleChildScrollView(
                      //   child: Text('Content'),
                      // ),
                    );
                  });
            } else if (firstnameController.text == "" ||
                lastnameController.text == "" ||
                emailController.text == "" ||
                confirmPasswordController.text == "" ||
                noHandphoneController.text == "") {
              showDialog(
                  context: context,
                  builder: (BuildContext builderContext) {
                    Future.delayed(Duration(seconds: 2), () {
                      // Navigator.of(context).pop();
                    });
                    return AlertDialog(
                      backgroundColor: Colors.white,
                      title: Center(
                        child: Text(
                          "ada kolom yang kosong",
                          style: ThemeFonts.textStyle400
                              .copyWith(fontSize: 17, color: Colors.black),
                        ),
                      ),
                      // content: SingleChildScrollView(
                      //   child: Text('Content'),
                      // ),
                    );
                  });
            } else {
              done();
            }
          });
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.all(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
        ),
        child: Ink(
          decoration: BoxDecoration(
              boxShadow: [BoxShadow(color: Colors.black38, blurRadius: 15.0)],
              borderRadius: BorderRadius.circular(30.0),
              gradient: LinearGradient(
                  colors: <Color>[Color(0xFFFF0000), Color(0xFF790001)])),
          child: Container(
            alignment: Alignment.center,
            constraints:
            BoxConstraints(maxWidth: double.infinity, minHeight: 50),
            child: Text(
              context.l10n.kSignUp,
              style: TextStyle(
                  color: Colors.white,
                  letterSpacing: 0.2,
                  fontFamily: "Sans",
                  fontSize: 18.0,
                  fontWeight: FontWeight.w800),
            ),
          ),
        ),
      ),
    );
  }



  void _showLoading(String text) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => Dialog(
        child: Padding(
          padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(),
              new Padding(
                padding: EdgeInsets.all(20),
                child: Text(text),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<Null> done() async {
    print("SignUp");
    try {
      _showLoading("Please Wait...");
      var fname = firstnameController.text;
      var lname = lastnameController.text;
      var email = emailController.text;
      var phone = noHandphoneController.text;
      var pass = passwordController.text;
      final AuthRepository authRepository = context.read();
      final res = await authRepository.signUp(
        email: email,
        firstName: fname,
        lastName: lname,
        phone: phone,
        password: pass,
      );

      if (res) {
        print("Registrasi Berhasil");
        showDialog(
            context: context,
            builder: (BuildContext builderContext) {
              Future.delayed(Duration(seconds: 1), () {
                clearAllFeild();
                // Navigator.of(context).pop();
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ),
                );
              });
              return AlertDialog(
                backgroundColor: Colors.white,
                title: Center(
                  child: Text(
                    "Register Berhasil Silahkan Login",
                    style: ThemeFonts.textStyle400
                        .copyWith(fontSize: 17, color: Colors.black),
                  ),
                ),
                // content: SingleChildScrollView(
                //   child: Text('Content'),
                // ),
              );
            });
      } else {
        print("Register Gagal");
        showDialog(
            context: context,
            builder: (BuildContext builderContext) {
              Future.delayed(Duration(seconds: 2), () {
                // Navigator.of(context).pop();
              });
              return AlertDialog(
                backgroundColor: Colors.white,
                title: Center(
                  child: Text(
                    "Register Gagal",
                    style: ThemeFonts.textStyle400
                        .copyWith(fontSize: 17, color: Colors.black),
                  ),
                ),
                // content: SingleChildScrollView(
                //   child: Text('Content'),
                // ),
              );
            });
      }
    } catch (e) {
      print(e);
    }
    return null;
  }
}
