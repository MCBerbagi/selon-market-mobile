import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:selon_market/selon_market.dart';

class ChooseLogin extends StatefulWidget {
  @override
  _ChooseLoginState createState() => _ChooseLoginState();

  static _ChooseLoginState? of(BuildContext context) => context
      .dependOnInheritedWidgetOfExactType<_ChooseLoginInherited>()!
      .state;
}

class _ChooseLoginState extends State<ChooseLogin>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  var tapLogin = 0;
  var tapSignup = 0;

  int _show = 0;
  set show(int value) {
    setState(() {
      _show = value;
    });
  }

  @override
  void initState() {
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300))
          ..addStatusListener((statuss) {
            if (statuss == AnimationStatus.dismissed) {
              setState(() {
                tapLogin = 0;
                tapSignup = 0;
              });
            }
          });
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<Null> _playAnimation() async {
    try {
      await animationController.forward();
      await animationController.reverse();
    } on TickerCanceled {}
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: buildBody(context),
      onWillPop: () async {
        if (_show > 0) {
          setState(() {
            _show = 0;
          });
          return false;
        }

        return true;
      },
    );
  }

  Widget buildBody(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);

    if (_show == 1) {
      return _ChooseLoginInherited(
        state: this,
        child: LoginScreen(),
      );
    }

    if (_show == 2) {
      return _ChooseLoginInherited(
        state: this,
        child: SignupScreen(),
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: _ChooseLoginInherited(
        state: this,
        child: Stack(
          children: <Widget>[
            Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height,
              child: CarouselSlider(
                unlimitedMode: true,
                enableAutoSlider: true,
                children: [
                  Image.asset(
                    'assets/img/girl.png',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    "assets/img/SliderLogin2.png",
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    'assets/img/SliderLogin3.png',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    "assets/img/SliderLogin4.png",
                    fit: BoxFit.cover,
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(),
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                      Color.fromRGBO(0, 0, 0, 0.3),
                      Color.fromRGBO(0, 0, 0, 0.4)
                    ],
                        begin: FractionalOffset.topCenter,
                        end: FractionalOffset.bottomCenter)),
                child: ListView(
                  padding: EdgeInsets.all(0.0),
                  children: <Widget>[
                    Stack(
                      alignment: AlignmentDirectional.bottomCenter,
                      children: <Widget>[
                        Stack(
                          alignment: AlignmentDirectional.bottomCenter,
                          children: <Widget>[
                            Container(
                              alignment: AlignmentDirectional.center,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: mediaQuery.padding.top + 50.0),
                                  ),
                                  Center(
                                    child: Hero(
                                      tag: "selon",
                                      child: Text(
                                        context.l10n.kTitle,
                                        style: TextStyle(
                                          fontFamily: 'Sans',
                                          fontWeight: FontWeight.w900,
                                          fontSize: 32.0,
                                          letterSpacing: 0.4,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 160.0,
                                          right: 160.0,
                                          top: mediaQuery.padding.top + 190.0,
                                          bottom: 10.0),
                                      child: Container(
                                        color: Colors.white,
                                        height: 0.5,
                                      )),
                                  Text(
                                    context.l10n.kHintChoseLogin,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.w200,
                                        fontFamily: "Sans",
                                        letterSpacing: 1.3),
                                  ),
                                  Padding(padding: EdgeInsets.only(top: 250.0)),
                                ],
                              ),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                tapLogin == 0
                                    ? Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          splashColor: Colors.white,
                                          onTap: () {
                                            setState(() {
                                              tapLogin = 1;
                                            });
                                            _playAnimation();
                                          },
                                          child: ButtonCustom(
                                            txt: context.l10n.kSignUp,
                                          ),
                                        ),
                                      )
                                    : AnimationSplashSignup(
                                        animationController: animationController
                                            .view as AnimationController,
                                      ),
                                Padding(padding: EdgeInsets.only(top: 15.0)),
                                Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        color: Colors.white,
                                        height: 0.2,
                                        width: 80.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                        child: InkWell(
                                          onTap: () {
                                            final AuthRepository repository =
                                                context.read();
                                            repository.skip();
                                          },
                                          child: Text(
                                            context.l10n.kOrSkip,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w100,
                                                fontFamily: "Sans",
                                                fontSize: 15.0),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        color: Colors.white,
                                        height: 0.2,
                                        width: 80.0,
                                      )
                                    ],
                                  ),
                                ),
                                Padding(padding: EdgeInsets.only(top: 70.0)),
                              ],
                            ),
                            tapSignup == 0
                                ? Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.white,
                                      onTap: () {
                                        setState(() {
                                          tapSignup = 1;
                                        });
                                        _playAnimation();
                                      },
                                      child: ButtonCustom(
                                        txt: context.l10n.kLogin,
                                      ),
                                    ),
                                  )
                                : AnimationSplashLogin(
                                    animationController: animationController
                                        .view as AnimationController,
                                  ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _ChooseLoginInherited extends InheritedWidget {
  final _ChooseLoginState? state;
  _ChooseLoginInherited({Key? key, required Widget child, this.state})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(_ChooseLoginInherited oldWidget) {
    return true;
  }
}

class ButtonCustom extends StatelessWidget {
  final String? txt;
  final GestureTapCallback? ontap;

  ButtonCustom({
    this.txt,
    this.ontap,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Colors.white,
        child: LayoutBuilder(builder: (context, constraint) {
          return Container(
            width: 300.0,
            height: 52.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.0),
                color: Colors.transparent,
                border: Border.all(color: Colors.white)),
            child: Center(
                child: Text(
              txt!,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 19.0,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Sans",
                  letterSpacing: 0.5),
            )),
          );
        }),
      ),
    );
  }
}

class AnimationSplashLogin extends StatefulWidget {
  AnimationSplashLogin({Key? key, required this.animationController})
      : animation = new Tween(
          end: 900.0,
          begin: 70.0,
        ).animate(CurvedAnimation(
            parent: animationController, curve: Curves.fastOutSlowIn)),
        super(key: key);

  final AnimationController animationController;
  final Animation animation;

  Widget _buildAnimation(BuildContext context, Widget? child) {
    return Padding(
      padding: EdgeInsets.only(bottom: 60.0),
      child: Container(
        height: animation.value,
        width: animation.value,
        decoration: BoxDecoration(
          color: Colors.white,
          shape: animation.value < 600 ? BoxShape.circle : BoxShape.rectangle,
        ),
      ),
    );
  }

  @override
  _AnimationSplashLoginState createState() => _AnimationSplashLoginState();
}

class _AnimationSplashLoginState extends State<AnimationSplashLogin> {
  @override
  Widget build(BuildContext context) {
    widget.animationController.addListener(() {
      if (widget.animation.isCompleted) {
        ChooseLogin.of(context)!.show = 1;
      }
    });
    return AnimatedBuilder(
      animation: widget.animationController,
      builder: widget._buildAnimation,
    );
  }
}

class AnimationSplashSignup extends StatefulWidget {
  AnimationSplashSignup({Key? key, required this.animationController})
      : animation = new Tween(
          end: 900.0,
          begin: 70.0,
        ).animate(CurvedAnimation(
            parent: animationController, curve: Curves.fastOutSlowIn)),
        super(key: key);

  final AnimationController animationController;
  final Animation animation;

  Widget _buildAnimation(BuildContext context, Widget? child) {
    return Padding(
      padding: EdgeInsets.only(bottom: 60.0),
      child: Container(
        height: animation.value,
        width: animation.value,
        decoration: BoxDecoration(
          color: Colors.white,
          shape: animation.value < 600 ? BoxShape.circle : BoxShape.rectangle,
        ),
      ),
    );
  }

  @override
  _AnimationSplashSignupState createState() => _AnimationSplashSignupState();
}

class _AnimationSplashSignupState extends State<AnimationSplashSignup> {
  @override
  Widget build(BuildContext context) {
    widget.animationController.addListener(() {
      if (widget.animation.isCompleted) {
        ChooseLogin.of(context)!.show = 2;
      }
    });
    return AnimatedBuilder(
      animation: widget.animationController,
      builder: widget._buildAnimation,
    );
  }
}
