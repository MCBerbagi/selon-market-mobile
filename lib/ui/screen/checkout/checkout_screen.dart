import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:selon_market/data/models/courier_new.dart';
import 'package:selon_market/selon_market.dart';

class CheckoutScreen extends StatefulWidget {
  CheckoutScreen({Key? key}) : super(key: key);

  @override
  _CheckoutScreenState createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  Checkout get data => ModalRoute.of(context)!.settings.arguments as Checkout;

  double get subtotal {
    double result = 0;
    result += data.items.fold(
      0,
          (previousValue, element) =>
      previousValue += (element.qty * element.cart.price),
    );
    return result;
  }

  double get total {
    double result = subtotal;
    if (selectedCourierService != null) {
      result += selectedCourierService!.finalRate!;
    }
    return result;
  }

  bool isProcessing = false;

  Widget buildSummary() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Total Belanja:',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            const Divider(),
            for (var item in data.items)
              Row(
                children: [
                  Text(item.qty.toString()),
                  const SizedBox(width: 16.0),
                  Text(item.cart.name),
                  Spacer(),
                  Text('Rp ' + (item.cart.price * item.qty).format()),
                ],
              ),
            const Divider(),
            Row(
              children: [
                Text(
                  'Subtotal',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Spacer(),
                Text(
                  'Rp ' + subtotal.format(),
                ),
              ],
            ),
            const Divider(),
            Row(
              children: [
                Text(
                  'Ongkos Kirim',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Spacer(),
                if (selectedCourierService != null)
                  Text(
                    'Rp ' + selectedCourierService!.finalRate!.format(),
                  )
                else
                  Text('Rp 0'),
              ],
            ),
            Row(
              children: [
                Text(
                  'Total',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Spacer(),
                Text(
                  'Rp ' + total.format(),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  ShippingAddress? _address;

  Widget buildShippingAddress() {
    late Widget child;
    if (_address == null) {
      child = Text('Pilih Lokasi');
    } else {
      child = Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(_address!.address),
          Text(_address!.city),
          Text(_address!.district),
          Text(_address!.zipCode),
        ],
      );
    }

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Pilih Alamat:',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            const Divider(),
            Row(
              children: [
                Expanded(
                  child: child,
                ),
                IconButton(
                  icon: Icon(CupertinoIcons.placemark_fill),
                  onPressed: () async {
                    AddressRepository repository = context.read();
                    final res =
                    await Navigator.pushNamed(context, '/pick-address');
                    if (res != null) {
                      _address = res as ShippingAddress;
                      data.outlet.areaId =
                      await repository.searchAreaId(outlet: data.outlet);
                      _couriers = await DeliveryService().getCouriers(
                          senderAreaId: data.outlet.areaId!,
                          senderCoordinate:
                          '${data.outlet.latitude}, ${data.outlet.longitude}',
                          receiverAreaId: _address!.areasId,
                          receiverCoordinate:
                          '${_address!.latitude}, ${_address!.longitude}',
                          itemWeight: 1,
                          itemLength: 20,
                          itemWidth: 10,
                          itemHeight: 10,
                          itemPrice: subtotal.toInt());
                      setState(() {});
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  CourierNew _couriers = CourierNew();
  // CourierNew? selectedCourier;
  LogisticService? selectedCourierService;

  Widget buildCouriers() {
    if (_couriers.code == null) {
      return Card();
    }

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Pilih Pengiriman:',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            const Divider(),
            // for (var courier in _couriers.data!.logistic!.regular!)
            (_couriers.data!.logistic!.regular == null)
                ? Container()
                : ExpansionTile(
              title: Text("Regular"),
              children: [
                for (var service in _couriers.data!.logistic!.regular!)
                  ListTile(
                    selectedTileColor: Colors.white,
                    selected: selectedCourierService == service,
                    dense: true,
                    title: Text(service.name!),
                    subtitle: Text(service.rateName!),
                    trailing: Text(service.finalRate!.format()),
                    onTap: () {
                      setState(() {
                        // selectedCourier = courier;
                        selectedCourierService = service;
                      });
                    },
                  )
              ],
            ),
            (_couriers.data!.logistic!.express == null)
                ? Container()
                : ExpansionTile(
              title: Text("Express"),
              children: [
                for (var service in _couriers.data!.logistic!.express!)
                  ListTile(
                    selectedTileColor: Colors.white,
                    selected: selectedCourierService == service,
                    dense: true,
                    title: Text(service.name!),
                    subtitle: Text(service.rateName!),
                    trailing: Text(service.finalRate!.format()),
                    onTap: () {
                      setState(() {
                        // selectedCourier = courier;
                        selectedCourierService = service;
                      });
                    },
                  )
              ],
            ),
            (_couriers.data!.logistic!.sameDay == null)
                ? Container()
                : ExpansionTile(
              title: Text("Same Day"),
              children: [
                for (var service in _couriers.data!.logistic!.sameDay!)
                  ListTile(
                    selectedTileColor: Colors.white,
                    selected: selectedCourierService == service,
                    dense: true,
                    title: Text(service.name!),
                    subtitle: Text(service.rateName!),
                    trailing: Text(service.finalRate!.format()),
                    onTap: () {
                      setState(() {
                        // selectedCourier = courier;
                        selectedCourierService = service;
                      });
                    },
                  )
              ],
            ),
            (_couriers.data!.logistic!.instant == null)
                ? Container()
                : ExpansionTile(
              title: Text("Instant"),
              children: [
                for (var service in _couriers.data!.logistic!.instant!)
                  ListTile(
                    selectedTileColor: Colors.white,
                    selected: selectedCourierService == service,
                    dense: true,
                    title: Text(service.name!),
                    subtitle: Text(service.rateName!),
                    trailing: Text(service.finalRate!.format()),
                    onTap: () {
                      setState(() {
                        // selectedCourier = courier;
                        selectedCourierService = service;
                      });
                    },
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }

  bool get isValid {
    // TODO: hapus return dan uncomment return dibawah ini jika AMANDA sudah ready
    // return true;
    return selectedCourierService != null;
  }

  String paymentMethod = availablePaymentMethods.first.channelId;
  Widget buildPaymentMethods() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Pilihan Pembayaran:',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            const Divider(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: DropdownButtonFormField<String>(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                ),
                items: [
                  for (var bank in availablePaymentMethods)
                    DropdownMenuItem(
                      value: bank.channelId,
                      child: Row(
                        children: [
                          Image.asset(
                            bank.logo,
                            width: 32,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Text(bank.bank),
                        ],
                      ),
                    ),
                ],
                value: paymentMethod,
                onChanged: (value) {
                  if (value == null) return;
                  setState(() {
                    paymentMethod = value;
                  });
                },
              ),
            ),
            Text(
              '*Harga Total belum termasuk biaya admin',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 10.0,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Color(0xFFFF0000)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'Checkout',
          style: TextStyle(
              fontFamily: "Gotik",
              fontSize: 18.0,
              color: Colors.black54,
              fontWeight: FontWeight.w700),
        ),
        elevation: 0.0,
      ),
      body: isProcessing
          ? Center(
        child: CircularProgressIndicator(),
      )
          : SingleChildScrollView(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildShippingAddress(),
            const SizedBox(height: 8.0),
            buildCouriers(),
            const SizedBox(height: 8.0),
            buildSummary(),
            const SizedBox(height: 8.0),
            buildPaymentMethods(),
            const SizedBox(height: 16.0),
            ElevatedButton(
              child: Text('BAYAR'),
              onPressed: !isValid
                  ? null
                  : () async {
                // TODO hapus if ini
                // if (selectedCourierService == null) {
                //   selectedCourier = Courier(
                //     name: 'TEST',
                //     services: [],
                //   );

                //   selectedCourierService = CourierService(
                //     name: 'TEST SERVICE',
                //     price: 0,
                //     description: 'Test',
                //   );
                // }

                try {
                  setState(() {
                    isProcessing = true;
                  });

                  OrderRepository repository = context.read();
                  final result = await repository.checkout(
                    checkout: data,
                    receiver: _address!,
                    // courier: selectedCourier!,
                    // courierService: selectedCourierService!,
                    logisticService: selectedCourierService!,
                    channelId: paymentMethod,
                  );

                  if (result != null) {
                    CartRepository cartRepository = context.read();
                    cartRepository.delete(
                      data.items.map((e) => e.cart).toList(),
                    );

                    Navigator.pushReplacementNamed(
                      context,
                      '/virtual-account',
                      arguments: result,
                    );
                  }
                } finally {
                  if (mounted) {
                    setState(() {
                      isProcessing = false;
                    });
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
