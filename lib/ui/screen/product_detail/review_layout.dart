import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:selon_market/selon_market.dart';

class ReviewsAllScreen extends StatefulWidget {
  @override
  _ReviewsAllScreenState createState() => _ReviewsAllScreenState();
}

class _ReviewsAllScreenState extends State<ReviewsAllScreen> {
  double rating = 3.5;
  int starCount = 5;

  var _detailText = TextStyle(
      fontFamily: "Gotik",
      color: Colors.black54,
      letterSpacing: 0.3,
      wordSpacing: 0.5);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          context.l10n.kReviewsAppBar,
        ),
        centerTitle: true,
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.black87,
            )),
        elevation: 0.0,
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, left: 20.0),
                  child: Text(
                    context.l10n.kReviewsAppBar,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                        fontFamily: "Popins",
                        fontWeight: FontWeight.w700),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0, left: 20.0),
                  child: Row(
                    children: <Widget>[
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            RatingBar(
                              itemSize: 25.0,
                              itemCount: 5,
                              initialRating: rating,
                              glowColor: Colors.yellow,
                              ignoreGestures: true,
                              onRatingUpdate: (value) {},
                              ratingWidget: RatingWidget(
                                empty: Icon(Icons.star_outline),
                                full: Icon(Icons.star),
                                half: Icon(Icons.star_half),
                              ),
                            ),
                            SizedBox(width: 5.0),
                            Text('8 Reviews')
                          ]),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                  child: _line(),
                ),
                ListTile(
                  leading: Container(
                    height: 45.0,
                    width: 45.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/avatars/avatar-4.jpg"),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.all(Radius.circular(50.0))),
                  ),
                  title: Row(
                    children: <Widget>[
                      RatingBar(
                        itemSize: 20.0,
                        itemCount: 5,
                        initialRating: rating,
                        glowColor: Colors.yellow,
                        onRatingUpdate: (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        },
                        ratingWidget: RatingWidget(
                          empty: Icon(Icons.star_outline),
                          full: Icon(Icons.star),
                          half: Icon(Icons.star_half),
                        ),
                      ),
                      SizedBox(width: 8.0),
                      Text(
                        context.l10n.kDate,
                        style: TextStyle(fontSize: 12.0),
                      )
                    ],
                  ),
                  subtitle: ExpansionTileReview(
                    title: Text(
                      context.l10n.kRatingReview,
                      style: _detailText,
                    ),
                    children: [
                      SizedBox(height: 10.0),
                      Text(
                        context.l10n.kRatingReview2,
                        style: _detailText,
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        context.l10n.kRatingReview,
                        style: _detailText,
                      ),
                    ],
//                              child: Text("Read More",style: _subHeaderCustomStyle.copyWith(fontSize: 13.0,color: Colors.blueAccent),
//                              textAlign: TextAlign.end,
//                              ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                  child: _line(),
                ),
                _buildRating(context.l10n.kDate, context.l10n.kRatingReview,
                    (rating) {
                  setState(() {
                    this.rating = rating;
                  });
                }, "assets/avatars/avatar-1.jpg"),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                  child: _line(),
                ),
                _buildRating(context.l10n.kDate, context.l10n.kRatingReview,
                    (rating) {
                  setState(() {
                    this.rating = rating;
                  });
                }, "assets/avatars/avatar-4.jpg"),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                  child: _line(),
                ),
                _buildRating(context.l10n.kDate, context.l10n.kRatingReview,
                    (rating) {
                  setState(() {
                    this.rating = rating;
                  });
                }, "assets/avatars/avatar-2.jpg"),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                  child: _line(),
                ),
                _buildRating(context.l10n.kDate, context.l10n.kRatingReview,
                    (rating) {
                  setState(() {
                    this.rating = rating;
                  });
                }, "assets/avatars/avatar-3.jpg"),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                  child: _line(),
                ),
                _buildRating(context.l10n.kDate, context.l10n.kRatingReview,
                    (rating) {
                  setState(() {
                    this.rating = rating;
                  });
                }, "assets/avatars/avatar-5.jpg"),
                SizedBox(
                  height: 10.0,
                ),
                _line(),
                Column(
                  children: <Widget>[
                    ExpansionTileCustomRatting(
                      title: _buildRating(
                          context.l10n.kDate, context.l10n.kRatingReview,
                          (rating) {
                        setState(() {
                          this.rating = rating;
                        });
                      }, "assets/avatars/avatar-6.jpg"),
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-1.jpg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-3.jpg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-2.jpg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-1.jpg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-5.jpg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-5.jpg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-5.jpg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-5.jpg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-5.jpg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 15.0, bottom: 7.0),
                          child: _line(),
                        ),
                        _buildRating(
                            context.l10n.kDate, context.l10n.kRatingReview,
                            (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        }, "assets/avatars/avatar-5.jpg"),
                      ],
//                              child: Text("Read More",style: _subHeaderCustomStyle.copyWith(fontSize: 13.0,color: Colors.blueAccent),
//                              textAlign: TextAlign.end,
//                              ),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(bottom: 40.0)),
              ],
            ),
          ),

          /// Get a class AppbarGradient
          /// This is a Appbar in home activity
//          Container(
//            height: 200.0,
//            width: double.infinity,
//            child: Column(
//              children: <Widget>[
//                Container(
//                  padding: EdgeInsets.only(top: statusBarHeight),
//                  height: 8.0 + statusBarHeight,
//                  width: double.infinity,
//                  decoration: BoxDecoration(
//                    /// gradient in appbar
//                      gradient: LinearGradient(
//                          colors: [
////                            const Color(0xFFA3BDED),
////                            const Color(0xFF6991C7),
//                          Colors.white
//                          ],
//                          begin: const FractionalOffset(0.0, 0.0),
//                          end: const FractionalOffset(1.0, 0.0),
//                          stops: [0.0, 1.0],
//                          tileMode: TileMode.clamp)),
////                  child: Center(child: Text("Review",style: TextStyle(color: Colors.white,fontSize: 18.0,fontFamily: "Popins",fontWeight: FontWeight.w600),)),
//                ),
//                Icon(Icons.arrow_back_ios,color: Colors.black87,size: 20.0,),
//              ],
//            ),
//          ),
        ],
      ),
    );
  }

  Widget _buildRating(
      String date, String details, Function changeRating, String image) {
    return ListTile(
      leading: Container(
        height: 45.0,
        width: 45.0,
        decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage(image), fit: BoxFit.cover),
            borderRadius: BorderRadius.all(Radius.circular(50.0))),
      ),
      title: Row(
        children: <Widget>[
          RatingBar(
            itemSize: 20.0,
            itemCount: starCount,
            initialRating: rating,
            glowColor: Colors.yellow,
            onRatingUpdate: changeRating as void Function(double),
            ratingWidget: RatingWidget(
              empty: Icon(Icons.star_outline),
              full: Icon(Icons.star),
              half: Icon(Icons.star_half),
            ),
          ),
          SizedBox(width: 8.0),
          Text(
            date,
            style: TextStyle(fontSize: 12.0),
          )
        ],
      ),
      subtitle: Text(
        details,
        style: _detailText,
      ),
    );
  }
}

Widget _line() {
  return Container(
    height: 0.9,
    width: double.infinity,
    color: Colors.black12,
  );
}
