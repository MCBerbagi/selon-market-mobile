import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:selon_market/selon_market.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class NoItemCart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return Container(
      width: 500.0,
      color: Colors.white,
      height: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding:
                    EdgeInsets.only(top: mediaQueryData.padding.top + 50.0)),
            Image.asset(
              "assets/imgIllustration/IlustrasiCart.png",
              height: 300.0,
            ),
            Padding(padding: EdgeInsets.only(bottom: 10.0)),
            Text(
              context.l10n.kCartNoItem,
              style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 18.5,
                  color: Colors.black26.withOpacity(0.2),
                  fontFamily: "Popins"),
            ),
          ],
        ),
      ),
    );
  }
}

class _CartScreenState extends State<CartScreen> {
  int size = 8;
  int plus = 1;

  int? selected;

  int value = 1;
  int pay = 0;
  int grandTotal = 0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Color(0xFFFF0000)),
        centerTitle: true,
        backgroundColor: Colors.red,
        title: Text(
          context.l10n.kCart,
          style: TextStyle(
              fontFamily: "Gotik",
              fontSize: 18.0,
              color: Colors.white,
              fontWeight: FontWeight.w700),
        ),
        elevation: 0.0,
      ),
      body: buildBody(),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        child:  TextButton(
          child: Text('CHECKOUT'),
          style: TextButton.styleFrom(
            primary: Colors.white,
            backgroundColor: Colors.redAccent,
            onSurface: Colors.grey,
          ),
          onPressed: (){},
          // onPressed: total > 0
          //     ? () {
          //   Navigator.pushNamed(
          //     context,
          //     '/checkout',
          //     arguments: data,
          //   );
          // }
          //     : null,
        ),
      ),
    );
  }

  Widget buildBody() {
    final repository = context.watch<CartRepository>();
    final _listcart = repository.cartList
      ..sort((a, b) => a.outlet.id!.compareTo(b.outlet.id!));

    final _cartGroups = <Outlet>[];
    for (Cart item in _listcart) {
      if (_cartGroups.any((element) => element.id == item.outlet.id)) {
        continue;
      }

      _cartGroups.add(item.outlet);
    }

    return RefreshIndicator(
      onRefresh: repository.refresh,
      child: ListView.builder(
        physics: AlwaysScrollableScrollPhysics(),
        padding: const EdgeInsets.all(4.0),
        itemCount: _cartGroups.length,
        itemBuilder: (context, index) {
          final item = _cartGroups.elementAt(index);
          final items = <Cart>[];

          for (var cart in _listcart) {
            if (cart.outlet.id == item.id &&
                !items.any((element) => element.cartId == cart.cartId)) {
              items.add(cart);
            }
          }
          return CartGroup(
            outlet: item,
            items: items,
          );
        },
        scrollDirection: Axis.vertical,
      ),
    );
  }

  Widget roundedButton(String buttonLabel, Color bgColor, Color textColor) {
    var loginBtn = new Container(
      padding: EdgeInsets.all(5.0),
      alignment: FractionalOffset.center,
      decoration: new BoxDecoration(
        color: bgColor,
        borderRadius: new BorderRadius.all(const Radius.circular(10.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: const Color(0xFF696969),
            offset: Offset(1.0, 1.0),
            blurRadius: 0.001,
          ),
        ],
      ),
      child: Text(
        buttonLabel,
        style: new TextStyle(
            color: textColor, fontSize: 20.0, fontWeight: FontWeight.bold),
      ),
    );
    return loginBtn;
  }
}

class CartGroup extends StatefulWidget {
  final Outlet outlet;
  final List<Cart> items;

  const CartGroup({
    Key? key,
    required this.outlet,
    required this.items,
  }) : super(key: key);

  @override
  _CartGroupState createState() => _CartGroupState();
}

class _CartGroupState extends State<CartGroup> {
  List<int> _selectedIds = [];
  Map<int, int> qty = {};

  Checkout get data {
    return Checkout(
      outlet: widget.outlet,
      items: [
        for (var item in widget.items.where(
          (element) => _selectedIds.contains(element.cartId),
        ))
          CheckoutItem(
            qty: qty[item.cartId] ?? 1,
            cart: item,
          ),
      ],
    );
  }

  double get total {
    return widget.items.fold(0.0, (previousValue, element) {
      if (_selectedIds.contains(element.cartId)) {
        previousValue += element.price * (qty[element.cartId] ?? 1);
      }

      return previousValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                widget.outlet.name ?? '',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18.0,
                ),
              ),
            ),
            for (var item in widget.items)
              Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                actions: <Widget>[
                  new IconSlideAction(
                    caption: context.l10n.kCartArchiveText,
                    color: Colors.red,
                    icon: Icons.archive,
                    onTap: () {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(context.l10n.kCartArchice),
                        duration: Duration(seconds: 2),
                        backgroundColor: Colors.red,
                      ));
                    },
                  ),
                ],
                secondaryActions: <Widget>[
                  new IconSlideAction(
                    key: Key(item.productId.toString()),
                    caption: context.l10n.kCartDelete,
                    color: Colors.red,
                    icon: Icons.delete,
                    onTap: () {
                      setState(() {});

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(context.l10n.kCartDeleted),
                        duration: Duration(seconds: 2),
                        backgroundColor: Colors.redAccent,
                      ));
                    },
                  ),
                ],
                child: _CartItem(
                  item: item,
                  isSelected: _selectedIds.contains(item.cartId),
                  onChanged: (value) {
                    setState(() {
                      if (value!) {
                        _selectedIds.add(item.cartId);
                      } else {
                        _selectedIds.remove(item.cartId);
                      }
                    });
                  },
                  qty: qty[item.cartId] ?? 1,
                  onQtyChanged: (value) {
                    setState(() {
                      qty[item.cartId] = value;
                    });
                  },
                ),
              ),
            const Divider(),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 8.0,
              ),
              child: Row(
                children: [
                  Text('Rp ${total.format()}'),
                  Spacer(),
                  TextButton(
                    child: Text('CHECKOUT'),
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.redAccent,
                      onSurface: Colors.grey,
                    ),
                    onPressed: total > 0
                        ? () {
                            Navigator.pushNamed(
                              context,
                              '/checkout',
                              arguments: data,
                            );
                          }
                        : null,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
    // return Card {
    //
    // }
  }
}

class _CartItem extends StatefulWidget {
  final Cart item;
  final bool isSelected;
  final ValueChanged<bool?>? onChanged;
  final int qty;
  final ValueChanged<int> onQtyChanged;

  _CartItem({
    Key? key,
    required this.item,
    required this.isSelected,
    this.onChanged,
    required this.qty,
    required this.onQtyChanged,
  }) : super(key: key);

  @override
  __CartItemState createState() => __CartItemState();
}

class __CartItemState extends State<_CartItem> {
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.1),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12.withOpacity(0.1),
                      blurRadius: 0.5,
                      spreadRadius: 0.1,
                    )
                  ],
                ),
                child: Image.network(
                  '${widget.item.productMedia!.first.mediaUrl!.replaceAll("/admin/", "/product/")}',
                  height: 64.0,
                  width: 64.0,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                '${widget.item.name}',
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontFamily: "Sans",
                                  color: Colors.black87,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                              Text(
                                '${widget.item.description}',
                                style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Checkbox(
                          value: widget.isSelected,
                          onChanged: widget.onChanged,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Text("Rp ${widget.item.price.format()}"),
                        ),
                        Container(
                          width: 112.0,
                          decoration: BoxDecoration(
                              color: Colors.white70,
                              border: Border.all(
                                  color: Colors.black12.withOpacity(0.1))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  if (widget.qty == 1) return;
                                  widget.onQtyChanged(widget.qty - 1);
                                },
                                child: Container(
                                  height: 30.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      border: Border(
                                          right: BorderSide(
                                              color: Colors.black12
                                                  .withOpacity(0.1)))),
                                  child: Center(child: Text("-")),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 18.0),
                                child: Text(
                                  widget.qty.toString(),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  widget.onQtyChanged(widget.qty + 1);
                                },
                                child: Container(
                                  height: 30.0,
                                  width: 28.0,
                                  decoration: BoxDecoration(
                                      border: Border(
                                          left: BorderSide(
                                              color: Colors.black12
                                                  .withOpacity(0.1)))),
                                  child: Center(child: Text("+")),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 8),
                        IconButton(
                          iconSize: 18,
                          icon: Icon(CupertinoIcons.delete),
                          onPressed: () {
                            CartRepository repository = context.read();
                            repository.delete([widget.item]);
                          },
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
