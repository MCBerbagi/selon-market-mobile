import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selon_market/selon_market.dart';

class LocationPicker extends StatefulWidget {
  LocationPicker({Key? key}) : super(key: key);

  @override
  _LocationPickerState createState() => _LocationPickerState();
}

class _LocationPickerState extends State<LocationPicker> {
  late LatLng currentPosition;
  bool isInitialized = false;

  final _txtStreet = TextEditingController();
  final _txtProvince = TextEditingController();
  final _txtCity = TextEditingController();
  final _txtDistrict = TextEditingController();
  final _txtSubDistrict = TextEditingController();
  final _txtZipCode = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _txtStreet.dispose();
    _txtProvince.dispose();
    _txtCity.dispose();
    _txtDistrict.dispose();
    _txtSubDistrict.dispose();
    _txtZipCode.dispose();
    super.dispose();
  }

  Future<void> initialize() async {
    var status = await Permission.locationWhenInUse.status;
    if (!status.isGranted) {
      status = await Permission.locationWhenInUse.request();
    }

    if (status.isGranted) {
      final position = await Geolocator.getCurrentPosition();
      currentPosition = LatLng(position.latitude, position.longitude);
      if (mounted)
        setState(() {
          isInitialized = true;
        });
    }
  }

  @override
  void initState() {
    super.initState();
    initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Color(0xFF6991C7)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'Pick Location',
          style: TextStyle(
              fontFamily: "Gotik",
              fontSize: 18.0,
              color: Colors.black54,
              fontWeight: FontWeight.w700),
        ),
        elevation: 0.0,
      ),
      body: !isInitialized
          ? buildPermissionRequest()
          : Column(
        children: [
          Expanded(
            child: Stack(
              fit: StackFit.expand,
              alignment: Alignment.center,
              children: [
                Positioned.fill(
                  child: GoogleMap(
                    myLocationButtonEnabled: true,
                    myLocationEnabled: true,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(
                        currentPosition.latitude,
                        currentPosition.longitude,
                      ),
                      zoom: 16.0,
                    ),
                    onCameraMove: (position) {
                      currentPosition = position.target;
                    },
                    onCameraIdle: () async {
                      final addresses = await GeocodingPlatform.instance
                          .placemarkFromCoordinates(
                        currentPosition.latitude,
                        currentPosition.longitude,
                      );

                      if (addresses.isEmpty) {
                        return;
                      }

                      final address = addresses.first;
                      _txtStreet.text = address.street!;
                      _txtProvince.text = address.administrativeArea!;
                      _txtCity.text = address.locality!;
                      _txtDistrict.text = address.subLocality!;
                      _txtSubDistrict.text = address.thoroughfare!;
                      _txtZipCode.text = address.postalCode!;
                      print(address.subLocality);
                      print(address.subAdministrativeArea);
                      print(address.subThoroughfare);
                      print(address.administrativeArea);
                    },
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 24.0),
                    child: Icon(
                      Icons.place,
                      size: 48.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Theme(
                data: Theme.of(context).copyWith(
                  inputDecorationTheme: InputDecorationTheme(
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                    filled: true,
                  ),
                ),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextFormField(
                        controller: _txtStreet,
                        decoration: InputDecoration(
                          labelText: 'Street',
                        ),
                        validator: (value) {
                          if (value?.isEmpty ?? true) {
                            return 'This field is required';
                          }

                          return null;
                        },
                      ),
                      const SizedBox(height: 4),
                      TextFormField(
                        controller: _txtProvince,
                        decoration: InputDecoration(
                          labelText: 'Province',
                        ),
                        validator: (value) {
                          if (value?.isEmpty ?? true) {
                            return 'This field is required';
                          }

                          return null;
                        },
                      ),
                      const SizedBox(height: 4),
                      TextFormField(
                        controller: _txtCity,
                        decoration: InputDecoration(
                          labelText: 'City',
                        ),
                        validator: (value) {
                          if (value?.isEmpty ?? true) {
                            return 'This field is required';
                          }

                          return null;
                        },
                      ),
                      const SizedBox(height: 4),
                      TextFormField(
                        controller: _txtDistrict,
                        decoration: InputDecoration(
                          labelText: 'District',
                        ),
                        validator: (value) {
                          if (value?.isEmpty ?? true) {
                            return 'This field is required';
                          }

                          return null;
                        },
                      ),
                      const SizedBox(height: 4),
                      TextFormField(
                        controller: _txtSubDistrict,
                        decoration: InputDecoration(
                          labelText: 'Subdistrict',
                        ),
                        validator: (value) {
                          if (value?.isEmpty ?? true) {
                            return 'This field is required';
                          }

                          return null;
                        },
                      ),
                      const SizedBox(height: 4),
                      TextFormField(
                        controller: _txtZipCode,
                        decoration: InputDecoration(
                          labelText: 'Zip Code',
                        ),
                        validator: (value) {
                          if (value?.isEmpty ?? true) {
                            return 'This field is required';
                          }

                          return null;
                        },
                      ),
                      const SizedBox(height: 4),
                      TextButton(
                        child: Text('USE LOCATION'),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            final result = ShippingAddress(
                                id: 1,
                                address: _txtStreet.text,
                                province: _txtProvince.text,
                                city: _txtCity.text,
                                district: _txtDistrict.text,
                                subDistrict: _txtSubDistrict.text,
                                zipCode: _txtZipCode.text,
                                latitude: currentPosition.latitude,
                                longitude: currentPosition.longitude,
                                isDefault: false,
                                areasId: 0
                            );

                            Navigator.pop(context, result);
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildPermissionRequest() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Location access is denied'),
        ],
      ),
    );
  }
}
