import 'package:flutter/material.dart';
import 'package:selon_market/selon_market.dart';

class NotificationScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          context.l10n.kNotification,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              color: Colors.black54,
              fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(
          color: const Color(0xFF6991C7),
        ),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body: buildBody(context),
    );
  }

  Widget buildBody(BuildContext context) {
    final _listbanner = context.watch<PromoRepository>().bannerList;
    return ListView.builder(
      itemCount: _listbanner.length,
      padding: const EdgeInsets.all(5.0),
      itemBuilder: (context, index) {
        return Dismissible(
            key: Key(_listbanner[index].bannerId.toString()),
            onDismissed: (direction) {},
            background: Container(
              color: Color(0xFF6991C7),
            ),
            child: Container(
              height: 88.0,
              child: Column(
                children: <Widget>[
                  Divider(height: 5.0),
                  ListTile(
                    title: Text(
                      '${_listbanner[index].title}',
                      style: TextStyle(
                          fontSize: 17.5,
                          color: Colors.black87,
                          fontWeight: FontWeight.w600),
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.only(top: 6.0),
                      child: Container(
                        width: 440.0,
                        child: Text(
                          '${_listbanner[index].promoUrl}',
                          style: new TextStyle(
                              fontSize: 15.0,
                              fontStyle: FontStyle.italic,
                              color: Colors.black38),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                    leading: Column(
                      children: <Widget>[
                        Container(
                          height: 40.0,
                          width: 40.0,
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(60.0)),
                            image: DecorationImage(
                              image: NetworkImage(
                                  '${_listbanner[index].imageUrl!.replaceAll("/admin/", "/product/")}'),
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Divider(height: 5.0),
                ],
              ),
            ));
      },
    );
  }
}
