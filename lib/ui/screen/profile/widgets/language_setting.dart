import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:selon_market/selon_market.dart';

class LanguageSettingScreen extends StatefulWidget {
  LanguageSettingScreen({Key? key}) : super(key: key);

  _LanguageSettingScreenState createState() => _LanguageSettingScreenState();
}

class _LanguageSettingScreenState extends State<LanguageSettingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          context.l10n.kLanguageSetting,
          style: TextStyle(
              fontFamily: "Gotik",
              fontWeight: FontWeight.w600,
              fontSize: 18.5,
              letterSpacing: 1.2,
              color: Colors.black87),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Color(0xFF6991C7)),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (_) => NetworkGiffyDialog(
                            image: Image.asset(
                              "assets/gif/earth.gif",
                              fit: BoxFit.cover,
                            ),
                            title: Text(context.l10n.kTitleCard,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: "Gotik",
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w600)),
                            description: Text(
                              context.l10n.kDescCard,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: "Popins",
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black26),
                            ),
                            onOkButtonPressed: () {
                              context.read<ConfigRepository>().setLocale('en');
                              Navigator.pop(context);
                            },
                          ));
                },
                child: _CardName(
                  flag: "assets/img/us.png",
                  title: context.l10n.kEnglish,
                )),
            InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (_) => NetworkGiffyDialog(
                            image: Image.asset(
                              "assets/gif/earth.gif",
                              fit: BoxFit.cover,
                            ),
                            title: Text(context.l10n.kTitleCard,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: "Gotik",
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w600)),
                            description: Text(
                              context.l10n.kDescCard,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: "Popins",
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black26),
                            ),
                            onOkButtonPressed: () {
                              context.read<ConfigRepository>().setLocale('ar');
                              Navigator.pop(context);
                            },
                          ));
                },
                child: _CardName(
                  flag: "assets/img/arab.png",
                  title: context.l10n.kArabic,
                )),
            InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (_) => NetworkGiffyDialog(
                            image: Image.asset(
                              "assets/gif/earth.gif",
                              fit: BoxFit.cover,
                            ),
                            title: Text(context.l10n.kTitleCard,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: "Gotik",
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w600)),
                            description: Text(
                              context.l10n.kDescCard,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: "Popins",
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black26),
                            ),
                            onOkButtonPressed: () {
                              context.read<ConfigRepository>().setLocale('zh');
                              Navigator.pop(context);
                            },
                          ));
                },
                child: _CardName(
                  flag: "assets/img/china.png",
                  title: context.l10n.kChinese,
                )),
            InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (_) => NetworkGiffyDialog(
                            image: Image.asset(
                              "assets/gif/earth.gif",
                              fit: BoxFit.cover,
                            ),
                            title: Text(context.l10n.kTitleCard,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: "Gotik",
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w600)),
                            description: Text(
                              context.l10n.kDescCard,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: "Popins",
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black26),
                            ),
                            onOkButtonPressed: () {
                              context.read<ConfigRepository>().setLocale('hi');
                              Navigator.pop(context);
                            },
                          ));
                },
                child: _CardName(
                  flag: "assets/img/india.png",
                  title: context.l10n.kHindi,
                )),
            InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (_) => NetworkGiffyDialog(
                            image: Image.asset(
                              "assets/gif/earth.gif",
                              fit: BoxFit.cover,
                            ),
                            title: Text(context.l10n.kTitleCard,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: "Gotik",
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w600)),
                            description: Text(
                              context.l10n.kDescCard,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: "Popins",
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black26),
                            ),
                            onOkButtonPressed: () {
                              context.read<ConfigRepository>().setLocale('id');
                              Navigator.pop(context);
                            },
                          ));
                },
                child: _CardName(
                  flag: "assets/img/indonesia.png",
                  title: context.l10n.kIndonesia,
                )),
            SizedBox(
              height: 70.0,
            )
          ],
        ),
      ),
    );
  }
}

class _CardName extends StatelessWidget {
  final String? title, flag;
  _CardName({this.title, this.flag});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
      child: Container(
        height: 80.0,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 10.0,
                  spreadRadius: 0.0)
            ]),
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 15.0),
          child: Row(children: <Widget>[
            Image.asset(
              flag!,
              fit: BoxFit.cover,
              height: 65.0,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              child: Text(
                title!,
                style: TextStyle(
                    fontFamily: "Popins",
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.3),
              ),
            )
          ]),
        ),
      ),
    );
  }
}
