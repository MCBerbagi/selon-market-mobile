import 'package:flutter/material.dart';
import 'package:selon_market/selon_market.dart';

class CallCenterScreen extends StatefulWidget {
  @override
  _CallCenterScreenState createState() => _CallCenterScreenState();
}

class _CallCenterScreenState extends State<CallCenterScreen> {
  var _txtCustomHead = TextStyle(
    color: Colors.black54,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  var _txtCustomSub = TextStyle(
    color: Colors.black26,
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    fontFamily: "Gotik",
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          context.l10n.kCallCenter,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 16.0,
              color: Colors.black54,
              fontFamily: "Gotik"),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Color(0xFF6991C7)),
        elevation: 0.0,
      ),
      body: Container(
          color: Colors.white,
          child: Center(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                "assets/icon/girlcallcenter.png",
                height: 175.0,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 14.0),
                child: Text(
                  context.l10n.kCallCenter1,
                  style: _txtCustomHead,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 5.0, right: 20.0, left: 20.0),
                child: Text(
                  context.l10n.kCallCenter2,
                  style: _txtCustomSub,
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, '/call-center-chat');
                  },
                  child: Center(
                    child: Container(
                      height: 50.0,
                      width: 280.0,
                      decoration: BoxDecoration(
                          color: Color(0xFF6991C7),
                          borderRadius:
                              BorderRadius.all(Radius.circular(25.0))),
                      child: Center(
                          child: Text(
                        context.l10n.kCallCenter3,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 16.0),
                      )),
                    ),
                  ),
                ),
              )
            ],
          ))),
    );
  }
}
