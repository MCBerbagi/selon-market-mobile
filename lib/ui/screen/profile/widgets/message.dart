import 'package:flutter/material.dart';
import 'package:selon_market/selon_market.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          context.l10n.kMessage,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 22.0,
              letterSpacing: 2.0,
              color: Colors.black54,
              fontFamily: "Berlin"),
        ),
        iconTheme: IconThemeData(
          color: const Color(0xFFFF0000),
        ),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
        width: 500.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 150.0)),
            Image.asset(
              "assets/img/notmessage.png",
              height: 200.0,
            ),
            Padding(padding: EdgeInsets.only(bottom: 20.0)),
            Text(
              context.l10n.kNoMessage,
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 19.5,
                  color: Colors.black54,
                  fontFamily: "Gotik"),
            ),
          ],
        ),
      ),
    );
  }
}
