import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:selon_market/data/repositories/profile_repository.dart';
import 'package:selon_market/selon_market.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

/// Custom Font
var _txt = TextStyle(
  color: Colors.black,
  fontFamily: "Sans",
);

/// Get _txt and custom value of Variable for Edit text
var _txtEdit = _txt.copyWith(color: Colors.black26, fontSize: 15.0);

/// Get _txt and custom value of Variable for Category Text
var _txtCategory = _txt.copyWith(
    fontSize: 14.5, color: Colors.black54, fontWeight: FontWeight.w500);

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final user = context.watch<ProfileRepository>().currentUser;

    var _profile = Padding(
      padding: EdgeInsets.only(
        top: 185.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 100.0,
                width: 100.0,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 2.5),
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage("assets/img/avatar.png"))),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(user.email),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(user.phone),
              ),
              InkWell(
                onTap: null,
                child: Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Text(
                    context.l10n.kEditProfile,
                    style: _txtEdit,
                  ),
                ),
              ),
            ],
          ),
          Container(),
        ],
      ),
    );

    return AnnotatedRegion(
      value: SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
        statusBarColor: Colors.transparent,
      ),
      child: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Stack(
            children: <Widget>[
              /// Setting Header Banner
              AnnotatedRegion(
                value: SystemUiOverlayStyle(
                  statusBarBrightness: Brightness.dark,
                  statusBarColor: Colors.transparent,
                ),
                child: Container(
                  height: 240.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/img/headerProfile.png"),
                        fit: BoxFit.cover),
                  ),
                ),
              ),

              /// Calling _profile variable
              _profile,
              Padding(
                padding: const EdgeInsets.only(top: 360.0),
                child: Column(
                  /// Setting Category List
                  children: <Widget>[
                    /// Call category class
                    // TODO: ganti icon address
                    _Category(
                      txt: context.l10n.kBrand,
                      padding: 35.0,
                      image: "assets/icon/truck1.png",
                      tap: () {
                        Navigator.pushNamed(context, '/address');
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    _Category(
                      txt: context.l10n.kLanguage,
                      padding: 44.0,
                      image: "assets/icon/language.png",
                      tap: () {
                        Navigator.pushNamed(context, '/language');
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    _Category(
                      txt: context.l10n.kMyOrders,
                      padding: 35.0,
                      image: "assets/icon/truck.png",
                      tap: () {
                        Navigator.pushNamed(context, '/orders');
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    _Category(
                      txt: context.l10n.kNotification,
                      padding: 50.0,
                      image: "assets/icon/notification.png",
                      tap: () {
                        Navigator.pushNamed(context, '/notification');
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    _Category(
                      padding: 53.0,
                      txt: context.l10n.kAboutApps,
                      image: "assets/icon/aboutapp.png",
                      tap: () {
                        Navigator.pushNamed(context, '/about');
                      },
                    ),
                    Padding (
                      padding: const EdgeInsets.only(
                        top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        final AuthRepository authRepository = context.read();
                        authRepository.signOut();
                      },

                      child: Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Container(
                          height: 50.0,
                          width: 1000.0,
                          color: Colors.white,
                          child: _Category(
                            padding: 38.0,
                            txt: context.l10n.kLogout,
                            image: "assets/icon/logout.png",
                          ),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 20.0)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/// Component category class to set list
class _Category extends StatelessWidget {
  final String? txt, image;
  final GestureTapCallback? tap;
  final double? padding;

  _Category({this.txt, this.image, this.tap, this.padding});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: tap,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 15.0, left: 30.0),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: padding!),
                  child: Image.asset(
                    image!,
                    height: 25.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: Text(
                    txt!,
                    style: _txtCategory,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
