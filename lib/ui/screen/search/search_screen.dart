import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:selon_market/selon_market.dart';

class SearchScreen extends StatefulWidget {
  final String? categoryName;

  const SearchScreen({Key? key, required this.categoryName}) : super(key: key);
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final _filter = TextEditingController();

  List names = [];
  List filteredNames = [];
  bool isStarted = false;

  @override
  void dispose() {
    _filter.dispose();
    super.dispose();
  }

  Widget _buildList() {
    final searchText = _filter.text;
    if (searchText.isEmpty) return Container();

    final _listProduk = context
        .watch<ProductRepository>()
        .list
        .where((element) => element.category == widget.categoryName)
        .toList()
        .where((element) {
          if (element.name!.toLowerCase().contains(searchText.toLowerCase())) {
            return true;
          }

          return false;
        })
        .take(5)
        .toList();

    return ListView.builder(
      shrinkWrap: true,
      itemCount: _listProduk.length,
      padding: const EdgeInsets.all(0.0),
      itemBuilder: (BuildContext context, int index) {
        final item = _listProduk.elementAt(index);
        return new ListTile(
          title: Text(_listProduk[index].name!),
          onTap: () {
            Navigator.pushReplacementNamed(
              context,
              '/product',
              arguments: item,
            );
          },
        );
      },
    );
  }

  Widget _productItem({String? title, image, price}) {
    return Container(
      width: 180,
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.grey.shade200,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: CachedNetworkImage(
                colorBlendMode: BlendMode.colorDodge,
                fit: BoxFit.cover,
                imageUrl: '$image',
                placeholder: (context, url) => CupertinoActivityIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          ),
          SizedBox(height: 8),
          Text(
            '$title',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '$price',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              ),
              Container(
                padding: EdgeInsets.all(1),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.grey.shade400,
                ),
                child: Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _productItemm({String? title, image, price}) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 4.0,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 16.0,
        vertical: 8.0,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 8.0,
            spreadRadius: 0.0,
          )
        ],
      ),
      child: Center(
        child: Text(
          '$title',
          overflow: TextOverflow.fade,
          style: TextStyle(color: Colors.black54, fontFamily: "Sans"),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    /// Sentence Text header "Hello i am Selon.........."
    var _textHello = Padding(
      padding: const EdgeInsets.only(right: 50.0, left: 20.0),
      child: Text(
        context.l10n.kSearchHello,
        style: TextStyle(
            letterSpacing: 0.1,
            fontWeight: FontWeight.w600,
            fontSize: 27.0,
            color: Colors.black54,
            fontFamily: "Gotik"),
      ),
    );

    /// Item TextFromField Search
    var _search = Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 15.0,
                  spreadRadius: 0.0)
            ]),
        child: Column(
          children: [
            TextField(
              controller: _filter,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                border: InputBorder.none,
                prefixIcon: Icon(
                  Icons.search,
                  color: Color(0xFFFF0000),
                  size: 28.0,
                ),
                hintText: context.l10n.kFindYouWant,
                hintStyle: TextStyle(
                    color: Colors.black54,
                    fontFamily: "Gotik",
                    fontWeight: FontWeight.w400),
              ),
              onChanged: (value) {
                setState(() {});
              },
            ),
            _buildList(),
          ],
        ),
      ),
    );

    final _listProduk = context.watch<ProductRepository>().list;
    var _favorite = Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Container(
        height: 300.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                left: 20.0,
                right: 20.0,
                bottom: 16.0,
              ),
              child: Text(
                context.l10n.kFavorite,
                style: TextStyle(fontFamily: "Gotik", color: Colors.black26),
              ),
            ),
            Expanded(
              child: ListView.separated(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                itemCount: _listProduk.length,
                separatorBuilder: (context, index) => const SizedBox(
                  width: 8.0,
                ),
                itemBuilder: (BuildContext context, index) {
                  final item = _listProduk.elementAt(index);

                  return GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        '/product',
                        arguments: item,
                      );
                    },
                    child: _productItem(
                      title: item.name,
                      image: item.productMedia!.first.thumbnailUrl,
                      price: "Rp ${item.price!.format()}",
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );

    /// Popular Keyword Item
    var _sugestedText = Container(
      height: 110.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0),
            child: Text(
              context.l10n.kPopularity,
              style: TextStyle(fontFamily: "Gotik", color: Colors.black26),
            ),
          ),
          const SizedBox(
            height: 8.0,
          ),
          Expanded(
            child: ListView.builder(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              scrollDirection: Axis.horizontal,
              physics: BouncingScrollPhysics(),
              itemCount: _listProduk.length,
              itemBuilder: (BuildContext context, index) {
                final item = _listProduk.elementAt(index);

                return GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      '/product',
                      arguments: item,
                    );
                  },
                  child: _productItemm(
                    title: item.name,
                    image: item.productMedia!.first.thumbnailUrl,
                    price: "Rp ${item.price!.format()}",
                  ),
                );
              },
            ),
          )
        ],
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0.0,
        title: Text(
          context.l10n.kSearch,
          style: TextStyle(fontFamily: "Sofia"),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.only(top: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _textHello,
                _search,
                _sugestedText,
                _favorite,
                Padding(padding: EdgeInsets.only(bottom: 30.0, top: 2.0))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/// Popular Keyword Item class
class _KeywordItem extends StatefulWidget {
  final String? title, title2;

  _KeywordItem({this.title, this.title2});

  @override
  _KeywordItemState createState() => _KeywordItemState();
}

class _KeywordItemState extends State<_KeywordItem> {
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 4.0, left: 3.0),
          child: Container(
            height: 29.5,
            width: 90.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4.5,
                  spreadRadius: 1.0,
                )
              ],
            ),
            child: Center(
              child: Text(
                widget.title!,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.black54, fontFamily: "Sans"),
              ),
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 15.0)),
        Container(
          height: 29.5,
          width: 90.0,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 4.5,
                spreadRadius: 1.0,
              )
            ],
          ),
          child: Center(
            child: Text(
              widget.title2!,
              style: TextStyle(
                color: Colors.black54,
                fontFamily: "Sans",
              ),
            ),
          ),
        ),
      ],
    );
  }
}

///Favorite Item Card
class _FavoriteItem extends StatefulWidget {
  final String? image, rating, salary, title, sale;

  _FavoriteItem({this.image, this.rating, this.salary, this.title, this.sale});

  @override
  _FavoriteItemState createState() => _FavoriteItemState();
}

class _FavoriteItemState extends State<_FavoriteItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 2.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            boxShadow: [
              BoxShadow(
                color: Color(0xFF656565).withOpacity(0.15),
                blurRadius: 4.0,
                spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
              )
            ]),
        child: Wrap(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 120.0,
                  width: 150.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(7.0),
                          topRight: Radius.circular(7.0)),
                      image: DecorationImage(
                          image: AssetImage(widget.image!), fit: BoxFit.cover)),
                ),
                Padding(padding: EdgeInsets.only(top: 15.0)),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    widget.title!,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        color: Colors.black54,
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 13.0),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 1.0)),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    widget.salary!,
                    style: TextStyle(
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 14.0),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 15.0, right: 15.0, top: 5.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            widget.rating!,
                            style: TextStyle(
                                fontFamily: "Sans",
                                color: Colors.black26,
                                fontWeight: FontWeight.w500,
                                fontSize: 12.0),
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                            size: 14.0,
                          )
                        ],
                      ),
                      Text(
                        widget.sale!,
                        style: TextStyle(
                            fontFamily: "Sans",
                            color: Colors.black26,
                            fontWeight: FontWeight.w500,
                            fontSize: 12.0),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
