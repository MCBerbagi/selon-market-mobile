import 'package:flutter/material.dart';
import 'package:selon_market/selon_market.dart';
import 'package:selon_market/ui/screen/order/widgets/invoice.dart';

class OrderScreen extends StatefulWidget {
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  Order get order {
    final id = ModalRoute.of(context)!.settings.arguments as int;
    return context
        .read<OrderRepository>()
        .orderList
        .firstWhere((element) => element.id == id);
  }

  static var _txtCustom = TextStyle(
    color: Colors.black54,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(
          context.l10n.kTrackMyOrder,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20.0,
              color: Colors.black54,
              fontFamily: "Gotik"),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Color(0xFFFFFFFF)),
        elevation: 0.0,
      ),
      body: LayoutBuilder(builder: (context, constraint) {
        return RefreshIndicator(
          onRefresh: context.watch<OrderRepository>().refresh,
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Container(
              color: Colors.white,
              width: 800.0,
              // height: constraint.maxHeight,
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 20.0, left: 25.0, right: 25.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      order.orderDate.format(),
                      style: _txtCustom,
                    ),
                    Padding(padding: EdgeInsets.only(top: 7.0)),
                    Text(
                      order.invoice,
                      style: _txtCustom,
                    ),
                    Padding(padding: EdgeInsets.only(top: 30.0)),
                    Text(
                      context.l10n.kOrder,
                      style: _txtCustom.copyWith(
                          color: Colors.black54,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w600),
                    ),
                    Padding(padding: EdgeInsets.only(top: 20.0)),
                    StepList(
                      items: [
                        // TODO: ganti icon-icon status
                        StepItem(
                          isCompleted: true,
                          child: _QeueuItem(
                            icon: "assets/img/cartOK.png",
                            txtHeader: 'Checkout',
                            txtInfo: 'Pesanan telah diteruskan kepada penjual',
                          ),
                        ),
                        StepItem(
                          isCompleted: order.paymentStatus == 'paid',
                          child: _QeueuItem(
                            icon: "assets/img/Visacard.png",
                            txtHeader: 'Payment',
                            txtInfo: 'Pembayaran telah diverifikasi',
                          ),
                        ),
                        StepItem(
                          isCompleted: false,
                          child: _QeueuItem(
                            icon: "assets/img/BagOK.png",
                            txtHeader: 'Packing',
                            txtInfo: 'Pesanan sedang dikemas',
                          ),
                        ),
                        StepItem(
                          isCompleted: false,
                          child: _QeueuItem(
                            icon: "assets/img/SHIPPING.png",
                            txtHeader: 'Shipping',
                            txtInfo: 'Pesanan sedang dikirim',
                          ),
                        ),
                        StepItem(
                          isCompleted: false,
                          child: _QeueuItem(
                            icon: "assets/img/Shakinghands.png",
                            txtHeader: 'Done',
                            txtInfo: 'Pesanan diterima',
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 24.0),
                    buildDestination(context),
                    const SizedBox(height: 16.0),

                    //Button Invoice
                    (order.paymentStatus == 'paid') ? InkWell(
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          '/invoice',
                          arguments: order.id,
                        );
                      },
                      child: Container(
                        height: 55.0,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(40.0))),
                        child: Center(
                          child: Text(
                            "Invoice",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 16.5,
                                letterSpacing: 2.0),
                          ),
                        ),
                      ),
                    ) : Container(),
                    const SizedBox(height: 24.0),
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }

  Widget buildDestination(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        boxShadow: [
          BoxShadow(
            color: Colors.black12.withOpacity(0.1),
            blurRadius: 4.5,
            spreadRadius: 1.0,
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            width: 80,
            height: 80,
            alignment: Alignment.center,
            child: Image.asset("assets/img/house.png"),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  context.l10n.kDelivery,
                  style: _txtCustom.copyWith(fontWeight: FontWeight.w700),
                ),
                Padding(padding: EdgeInsets.only(top: 5.0)),
                Text(
                  order.customerName,
                  style: _txtCustom.copyWith(
                      fontWeight: FontWeight.w600,
                      fontSize: 12.0,
                      color: Colors.black38),
                ),
                Padding(padding: EdgeInsets.only(top: 2.0)),
                Text(
                  order.customerPhone,
                  style: _txtCustom.copyWith(
                      fontWeight: FontWeight.w400,
                      fontSize: 12.0,
                      color: Colors.black38),
                  overflow: TextOverflow.ellipsis,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class StepList extends StatelessWidget {
  final List<StepItem> items;
  const StepList({
    Key? key,
    required this.items,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        for (var item in items) buildItem(item),
      ],
    );
  }

  Widget buildItem(StepItem child) {
    return IntrinsicHeight(
      child: Row(
        children: [
          SizedBox(
            width: 48,
            child: Column(
              children: [
                if (items.indexOf(child) == 0)
                  SizedBox(
                    height: 12,
                  )
                else
                  SizedBox(
                    height: 12,
                    child: CustomPaint(
                      painter: DashedLinePainter(
                        color: child.isCompleted ? Colors.green : Colors.grey,
                      ),
                    ),
                  ),
                Container(
                  width: 24,
                  height: 24,
                  decoration: BoxDecoration(
                    color:
                        child.isCompleted ? Colors.green : Colors.transparent,
                    border: Border.all(
                      color: child.isCompleted ? Colors.green : Colors.grey,
                      width: 2.0,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                if (items.indexOf(child) == (items.length - 1))
                  Spacer()
                else
                  Expanded(
                    child: CustomPaint(
                      painter: DashedLinePainter(
                        color: Colors.grey,
                      ),
                    ),
                  ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: child.child,
            ),
          ),
        ],
      ),
    );
  }
}

class DashedLinePainter extends CustomPainter {
  final Color color;
  DashedLinePainter({
    required this.color,
  });

  @override
  void paint(Canvas canvas, Size size) {
    double dashWidth = 2, dashSpace = 4, startX = 0;
    final paint = Paint()
      ..color = Colors.green
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 1;
    while (startX < size.height) {
      canvas.drawLine(Offset(0, startX), Offset(0, startX + dashWidth), paint);
      startX += dashWidth + dashSpace;
    }
  }

  @override
  bool shouldRepaint(DashedLinePainter oldDelegate) =>
      oldDelegate.color != color;
}

class StepItem {
  final bool isCompleted;
  final Widget child;
  StepItem({
    required this.isCompleted,
    required this.child,
  });
}

class _QeueuItem extends StatelessWidget {
  static var _txtCustomOrder = TextStyle(
    color: Colors.black45,
    fontSize: 13.5,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  final String? icon, txtHeader, txtInfo;

  _QeueuItem({
    this.icon,
    this.txtHeader,
    this.txtInfo,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: 56,
          height: 56,
          alignment: Alignment.topCenter,
          child: Image.asset(icon!),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(txtHeader!, style: _txtCustomOrder),
              Text(
                txtInfo!,
                style: _txtCustomOrder.copyWith(
                  fontWeight: FontWeight.w400,
                  fontSize: 12.0,
                  color: Colors.black38,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
