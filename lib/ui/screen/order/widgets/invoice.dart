import 'dart:io';
import 'dart:typed_data';
import 'package:flutter_share/flutter_share.dart';
import 'package:path_provider/path_provider.dart';
import 'package:selon_market/selon_market.dart';
import 'package:share_extend/share_extend.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:selon_market/data/data.exports.dart';
import 'package:share_plus/share_plus.dart';

Widget titleValue(String title, String value) => Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
          ),
          const SizedBox(
            width: 25,
          ),
          Expanded(
            child: Text(
              value,
              softWrap: true,
              textAlign: TextAlign.end,
            ),
          ),
        ],
      ),
    );

class Invoice extends StatefulWidget {
  @override
  _InvoiceState createState() => _InvoiceState();
}

class _InvoiceState extends State<Invoice> {
  Order get order {
    final id = ModalRoute.of(context)!.settings.arguments as int;
    return context
        .read<OrderRepository>()
        .orderList
        .firstWhere((element) => element.id == id);
  }

  bool? hideButton;

  //Create an instance of ScreenshotController
  ScreenshotController screenshotController = ScreenshotController();

  //TO-DO: fitur share
  Future<void> shareScreenshot() async {
    Directory? directory;
    if (Platform.isAndroid) {
      directory = await getExternalStorageDirectory();
    } else {
      directory = await getApplicationDocumentsDirectory();
    }
    // final String localPath =
        // '${directory!.path}/${DateTime.now().toIso8601String()}.png';
    // final imageFile = await screenshotController.captureAndSave(localPath);

    final imageFile = await screenshotController.capture();
    final image =
        await File('${directory!.path}/${DateTime.now().toIso8601String()}.png')
            .create();
    await image.writeAsBytes(imageFile!);

    await Future.delayed(Duration(seconds: 1));

    // await FlutterShare.shareFile(
    //     title: order.invoice, filePath: localPath, fileType: 'image/png');
    // final RenderBox box = context.findRenderObject() as RenderBox;
    // if (localPath.isNotEmpty) {
    //   await Share.shareFiles([localPath],
    //       text: "INVOICE",
    //       subject: order.invoice,
    //       sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    // } else {
    //   await Share.share("INVOICE",
    //       subject: order.invoice,
    //       sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    // }

    ShareExtend.share(image.path, "image");
    setState(() {
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Screenshot(
      controller: screenshotController,
      child: Scaffold(
        body: Container(
          height: double.infinity,
          child: Center(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    Center(
                      child: Text(
                        "INVOICE",
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.bold),
                      ),
                    ),
                    const SizedBox(
                      height: 18,
                    ),
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Image.asset(
                                'assets/img/Logo.png',
                                scale: 7.5,
                              ),
                            ),
                            const SizedBox(
                              height: 24,
                            ),
                            titleValue("Nomor Invoice", order.invoice),
                            titleValue("Tanggal", order.orderDate.format()),
                            const SizedBox(
                              height: 24,
                            ),
                            Text(
                              "Tujuan Pengiriman",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14),
                            ),
                            DeliveryDestination(
                              order: order,
                            ),
                            const SizedBox(
                              height: 24,
                            ),
                            Text(
                              "Ringkasan Pembayaran",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14),
                            ),
                            PaymentSummary(order: order),
                            Divider(),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Total Bayar",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Text("Rp" + order.transactionAmount.format(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    //Button Invoice
                    (hideButton == true)
                        ? Container()
                        : InkWell(
                            onTap: () async {
                              setState(() {
                                hideButton = true;
                              });
                              shareScreenshot();
                            },
                            child: Container(

                              height: 45.0,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.red.shade700,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(40.0))),
                              child: Center(
                                child: Text(
                                  "Share",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16.5,
                                      letterSpacing: 2.0),
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class DeliveryDestination extends StatelessWidget {
  final Order order;

  const DeliveryDestination({Key? key, required this.order}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        titleValue("Nama", order.customerName),
        titleValue(
            "Alamat",
            order.customerAddress!.address +
                ", " +
                order.customerAddress!.district +
                ", " +
                order.customerAddress!.city +
                ", " +
                order.customerAddress!.province +
                "\n" +
                order.customerPhone),
      ],
    );
  }
}

class PaymentSummary extends StatelessWidget {
  final Order order;

  const PaymentSummary({Key? key, required this.order}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var orderData in order.productOrdered)
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Text(
                    orderData.productName + "  x " + orderData.qty.toString(),
                    softWrap: true,
                    textAlign: TextAlign.start,
                  ),
                ),
                const SizedBox(
                  width: 25,
                ),
                Text("Rp" + orderData.totalPrice.format()),
              ],
            ),
          ),
        Divider(),
        titleValue("Ongkos Kirim", "Rp" + order.deliveryPrice.format()),
        Divider(),
        titleValue("Biaya Admin", "Rp" + order.adminFee.format()),
      ],
    );
  }
}
