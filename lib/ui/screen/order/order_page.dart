import 'package:flutter/material.dart';
import 'package:selon_market/selon_market.dart';

class OrderPage extends StatefulWidget {
  OrderPage({Key? key}) : super(key: key);

  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          context.l10n.kTrackMyOrder,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20.0,
              color: Colors.white,
              fontFamily: "Gotik"),
        ),
        centerTitle: true,
        backgroundColor: Colors.red,
        iconTheme: IconThemeData(color: Color(0xFFFFFFFF)),
        elevation: 0.0,
      ),
      body: buildBody(),
    );
  }

  buildBody() {
    final OrderRepository repository = context.watch();

    return RefreshIndicator(
      onRefresh: repository.refresh,
      child: ListView.builder(
        physics: AlwaysScrollableScrollPhysics(),
        padding: const EdgeInsets.all(8.0),
        itemBuilder: (context, index) {
          final item = repository.orderList.elementAt(index);
          return Card(
            child: ExpansionTile(
              iconColor: Colors.white,
              title: Text(item.invoice),
              trailing: Text(item.paymentStatus),
              subtitle: Text(item.orderStatus),
              childrenPadding: const EdgeInsets.all(8.0),
              expandedCrossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  'Items',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                for (var product in item.productOrdered)
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 4.0,
                    ),
                    child: Row(
                      children: [
                        Text(product.qty.toString()),
                        const SizedBox(
                          width: 8.0,
                        ),
                        Text(product.productName),
                        Spacer(),
                        Text(product.totalPrice.format()),
                      ],
                    ),
                  ),
                const Divider(),
                Row(
                  children: [
                    Text(
                      'Subtotal',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Spacer(),
                    Text(
                      (item.transactionAmount -
                              item.deliveryPrice -
                              item.adminFee)
                          .toDouble()
                          .format(),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'Admin Fee',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Spacer(),
                    Text(
                      item.adminFee.format(),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'Delivery Fee',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Spacer(),
                    Text(
                      item.deliveryPrice.format(),
                    ),
                  ],
                ),
                const Divider(),
                Row(
                  children: [
                    Text(
                      'Total',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Spacer(),
                    Text(
                      item.transactionAmount.format(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                const Divider(),
                Align(
                  alignment: Alignment.centerRight,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.redAccent,
                      onSurface: Colors.grey,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(
                        context,
                        '/order-detail',
                        arguments: item.id,
                      );
                    },
                    child: Text('TRACK ORDER'),
                  ),
                ),
              ],
            ),
          );
        },
        itemCount: repository.orderList.length,
      ),
    );
  }
}
